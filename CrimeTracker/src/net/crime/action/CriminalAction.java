//Author Alekhya Chenupati

package net.crime.action;

import java.util.ArrayList;

import net.crime.objects.Criminal;
import net.crime.service.CriminalService;


public class CriminalAction {
	
	private ArrayList<Criminal> criminalList;
	public int criminalIdSelected;
	public int criminalId;
	public int idSelected;
	public int crimIdSelected;
	public ArrayList<Integer> otherCaseIds;
	public String criminalFirstName;
	 public String criminalLastName;
	 public String criminalGender;
	 public String criminalDescription;
	
	
	public String getAllCriminals(){
    	CriminalService criminalServ=new CriminalService();
    	ArrayList<Criminal> crmnlsList=criminalServ.getCriminalList(); 
    	if(crmnlsList.size()!=0)
    	{
    		setCriminalList(crmnlsList);
    				   
    		return "success";
    	}
	  else
	  {
		  System.out.println("No criminals to display");
		  return "NoCriminals";
	  }
     }

	public ArrayList<Criminal> getCriminalList() {
		return criminalList;
	}

	public void setCriminalList(ArrayList<Criminal> criminalList) {
		this.criminalList = criminalList;
	}


	
	public String deleteCriminals()
	{	CriminalService crimiService=new CriminalService();
		String status= crimiService.deleteCriminal(criminalIdSelected);
		   if(status.equals("success"))
		      return status;
		   else
			   return "failure";
	}
	
	public String editCriminals()
	{	CriminalService crimService=new CriminalService();
	ArrayList<Criminal> crmnlsList=crimService.editCriminal(criminalIdSelected); 
	if(crmnlsList.size()!=0)
	{
		setCriminalList(crmnlsList);
		return "success";
	}
  else
  {
	  System.out.println("No criminals to display");
	  return "NoCriminals";
  }
		
	}
	
	
	public String saveEditedCriminals()
	{	
	CriminalService crimService=new CriminalService();
	System.out.println("inside save edited crimidsele="+criminalId);
	System.out.println("fn="+criminalFirstName);
	String stat=crimService.saveThisCriminal(criminalId,criminalFirstName,criminalLastName,criminalGender,criminalDescription); 
	//if(stat="success")
	//{
		//setCriminalList(crmnlsList);
		return "getCriminals";
	//}
  //else
  //{
	//  System.out.println("No criminals to display");
	 // return "NoCriminals";
  //}
		
	}
	
	
	public String addACriminal()
	{
	
		CriminalService crimService=new CriminalService();
		String insertStatus=crimService.insertCriminal(criminalFirstName,criminalLastName,criminalGender,criminalDescription);
		
		String insertStatus2=crimService.insertCrime(idSelected);
	
		return insertStatus;
		
	}
	public String getCriminalsAssociated()
	{
		
		//System.out.println("inside getcriminals associatec caseid="+idSelected);
		CriminalService crmServ=new CriminalService();
		ArrayList<Criminal> crmnlsAssList=crmServ.selectAssociatedCriminals(idSelected); 
    	if(crmnlsAssList.size()!=0)
    	{
    		setCriminalList(crmnlsAssList);		   
    		return "success";
    	}
	  else
	  {
		  System.out.println("No criminals to display");
		  return "nocriminals";
	  }
     }
	
	public String fileNewCase()
	{
		System.out.println("Filing new case on criminal");
		CriminalService crimService=new CriminalService();
		String insertStatus=crimService.insertToCrime(otherCaseIds,crimIdSelected);
		System.out.println("insertststus="+insertStatus);
		return insertStatus;
		
	}
	public String delCriminalfromCrime()
	{
		CriminalService crimService=new CriminalService();
		System.out.println("idselected="+idSelected);
		String status= crimService.deleteCrimfromCrime(idSelected,criminalIdSelected);
		   if(status.equals("success"))
		      return status;
		   else
			   return "failure";
		
	}
	
	public String delCasefromCrime()
	{
		CriminalService crimService=new CriminalService();
		System.out.println("idselected="+idSelected);
		String status= crimService.deleteCrimfromCrime(idSelected,criminalIdSelected);
		   if(status.equals("success"))
		      return status;
		   else
			   return "failure";	
	}
}
