package net.crime.action;



import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

import net.crime.service.Authentication;
import net.crime.service.PoliceService;
import net.crime.objects.Police;
import net.crime.objects.Specialization;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;


public class PoliceAction extends ActionSupport implements ModelDriven<Police> {
	
	private Police police = new Police();
    private List<Police> policeList;
  

    private HashMap<Integer, Specialization> specialties = new HashMap<Integer, Specialization>();
    private PoliceService policeService = new PoliceService();
    
    @Override
    public Police getModel() {
    	ArrayList<Specialization> sl = policeService.getAllSpecialties();
    	for(Specialization s : sl) {
    	  specialties.put(new Integer(s.getSid()),s);
    	}
	
    	return police;
    }
    
    public PoliceAction() {
    	/**
    	ArrayList<Specialization> sl = policeService.getAllSpecialties();
    	for(Specialization s : sl) {
    	  specialties.put(s.getSid(),s);
    	}
    	*/
    
		
    }
    
    public String delete() {
    	
      HttpServletRequest request = 
    	(HttpServletRequest) ActionContext.getContext().get( ServletActionContext.HTTP_REQUEST);
    
      
      int id = Integer.parseInt( request.getParameter("policeId") );
      
      policeService.deletePolice(id);
      return SUCCESS;
    	
    }
    
    public String edit() {
        HttpServletRequest request = 
            	(HttpServletRequest) ActionContext.getContext().get( ServletActionContext.HTTP_REQUEST);
            
        int id = Integer.parseInt( request.getParameter("policeId") );
        
        police = policeService.listPoliceById(id);
        return SUCCESS;
    }
     
    
    public String saveOrUpdate() {
    
    	if(police.getPoliceId() != 0 ) {
    		
    	   policeService.updatePolice(police);
    	} else {
    		policeService.addPolice(police);
    	}
    	return SUCCESS;
    }
    
    public List<Police> getPoliceList() {
    	return this.policeList;
    }
    
    public void setPoliceList(List<Police> policeList) {
    	this.policeList = policeList;
    }
    
   
    
 
    public HashMap getSpecialties() {
    	
    	return specialties;
    }
    
    public void setSpecialties(HashMap specialties) {
    	
    	this.specialties = specialties;
    }
    

    
	public String listPolice() {
	
		this.policeList = policeService.showPolice();
		return SUCCESS;
		
	}
	
	public Police getPolice() {
	  return police;
	}
	
	public void setPolice(Police police) {
		this.police = police;
	}
}