package net.crime.action;

import java.util.ArrayList;

import net.crime.objects.Case;
import net.crime.objects.Criminal;
import net.crime.objects.Police;
import net.crime.service.CaseService;

public class CaseAction {
	
	private ArrayList<Case> caseList;
	private ArrayList<Police> officerList;
	private ArrayList<Criminal> suspectList;
	private ArrayList<String> pics;
	private int idSelected;
	private ArrayList<Integer> policeIds;
	
	//Alekhya
		public ArrayList<Integer> allCaseIds;
		public ArrayList<Integer> otherCases;
		public int crimIdSelected;
		
	
	private String happenedOn;
	private String closedOn;
	private String location;
	private String description;
	private String status;
	
	private ArrayList<Integer> cases;
	private ArrayList<Integer> polices;
	
    public String getAllCases(){
	   CaseService caseService=new CaseService();
	   ArrayList<Case> cl=caseService.getCaseList();
	   if(cl.size()!=0)
	      {
		   setCaseList(cl);
		   System.out.print(getCaseList().size());		   
		   return "success";
	      }
	   else
		   return "failure";
	   
       }

	public ArrayList<Case> getCaseList() {
		return caseList;
	}

	public void setCaseList(ArrayList<Case> caseList) {
		this.caseList = caseList;
	}

	public int getIdSelected() {
		return idSelected;
	}

	public void setIdSelected(int idSelected) {
		this.idSelected = idSelected;
	}
    
	public String getOfficersSuspects()
	{
		int csId=getIdSelected();
		
		CaseService caseService=new CaseService();
		ArrayList<Police> ol=caseService.getAssignedOfficerList(csId);
		   if(ol.size()!=0)
		      {
			   setOfficerList(ol);			   
			   setSuspectList(caseService.getSuspectList(csId));			   
			   return "success";
		      }
		   else
			   return "failure";
		
	}

	public ArrayList<Police> getOfficerList() {
		return officerList;
	}

	public void setOfficerList(ArrayList<Police> officerList) {
		this.officerList = officerList;
	}

	public String getHappenedOn() {
		return happenedOn;
	}

	public void setHappenedOn(String happenedOn) {
		this.happenedOn = happenedOn;
	}

	public String getClosedOn() {
		return closedOn;
	}

	public void setClosedOn(String closedOn) {
		this.closedOn = closedOn;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
    
	public String createCase()
	{
		CaseService caseService=new CaseService();
		
		if(caseService.create(getHappenedOn(),getClosedOn(), getLocation(), getDescription(),getStatus()))
		   return "success";
		else
			return "failure";
		
		
	}
	
	public String deleteCase()
	{
        CaseService caseService=new CaseService();
		
		if(caseService.delete(getIdSelected()))
		   return "success";
		else
			return "failure";
	}
	
    public String getCasePoliceLists()
    {
    	ArrayList<Case> tempCaseList;
    	ArrayList<Police> tempOfficerList;
    	CaseService caseService=new CaseService();
    	tempCaseList=caseService.getCases();
    	tempOfficerList=caseService.getPolices();
    	setCaseList(tempCaseList);
    	setOfficerList(tempOfficerList);
    	
    	ArrayList<Integer> tempCases=new ArrayList<Integer>();
    	ArrayList<Integer> tempPolices=new ArrayList<Integer>();
    	
    	for(Case cs:tempCaseList)
    	{
    		int cid=cs.getCaseId();
    		tempCases.add(cid);
    	}
    	for(Police pl:tempOfficerList)
    	{
    		int pid=pl.getPoliceId();
    		tempPolices.add(pid);
    	}
    	setCases(tempCases);
    	setPolices(tempPolices);
    	
    	return "success";
    }

	public ArrayList<Integer> getCases() {
		return cases;
	}

	public void setCases(ArrayList<Integer> cases) {
		this.cases = cases;
	}

	public ArrayList<Integer> getPolices() {
		return polices;
	}

	public void setPolices(ArrayList<Integer> polices) {
		this.polices = polices;
	}

	public ArrayList<Integer> getPoliceIds() {
		return policeIds;
	}

	public void setPoliceIds(ArrayList<Integer> policeIds) {
		this.policeIds = policeIds;
	}
	
	public String policeAssignment(){		
		CaseService caseService=new CaseService();
		if(caseService.assign(getIdSelected(),getPoliceIds()))
			return "success";
		else
			return "failure";
	}
	
	public String  findCase(){
		CaseService caseService=new CaseService();
		ArrayList<Case> csList=caseService.find(getIdSelected());
		
		if(csList.size()>0)
		{
			setCaseList(csList);
			for(Case cs:csList)
			{
				setHappenedOn(cs.getHappenedOn());
				setClosedOn(cs.getClosedOn());
				setLocation(cs.getLocation());
				setDescription(cs.getDescription());
				setStatus(cs.getStatus());
			}
			
			return "success";
		}
		else
			return "failure";
	}
	
	public String  updateCase(){
		
		CaseService caseService=new CaseService();	
		if(caseService.update(getHappenedOn(), getClosedOn(), getLocation(), getDescription(), getStatus(), getIdSelected()))		
		    return "success";
		else
			return "failure";
		
	}

	public ArrayList<Criminal> getSuspectList() {
		return suspectList;
	}

	public void setSuspectList(ArrayList<Criminal> suspectList) {
		this.suspectList = suspectList;
	}
	
	public ArrayList<String> getPics()
	{
		return this.pics;
	}

	public void setPics(ArrayList<String> pics) {
		this.pics = pics;
	}
	
	public String getPictures()
	{
		CaseService caseService=new CaseService();
		ArrayList<String> picLocation=caseService.getAllPicture(getIdSelected());
		if(picLocation.size()!=0)
		{
			setPics(picLocation);
			return "success";
		}
		else
			return "failure";
	}
	//Alekhya
	public void setCaseIds(ArrayList<Integer> allCaseIds)
	{
		this.allCaseIds=allCaseIds;
	}

	//Alekhya
		public String getAllCaseIds()
		{
			CaseService caseService=new CaseService();
			System.out.println("I am inside get all cases");
			  // ArrayList<Case> cl=caseService.getCaseIds();
			allCaseIds=caseService.getCaseIds();
			  // System.out.print("size is"+allCaseIds.size());
			   if(allCaseIds.size()!=0)
			      {
				   setCaseIds(allCaseIds);
				   System.out.print(allCaseIds.size());
				   return "success";
				  }
			   else
			   return "failure";
		}
		//Alekhya
		public String getCasesInvolved()
		{
			CaseService csServ=new CaseService();
			ArrayList<Case> csesinvolList=csServ.selectInvolvedCases(crimIdSelected); 
			 otherCases=csServ.selectOtherCases(crimIdSelected);
			System.out.println("crimidselected="+crimIdSelected);
	    	if(csesinvolList.size()!=0)
	    	{
	    		setCaseList(csesinvolList);		   
	    		return "success";
	    	}
		  else
		  {
			  System.out.println("No cases to display");
			  return "failure";
		  }
	     }
	
}
