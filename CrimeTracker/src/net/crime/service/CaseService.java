package net.crime.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import net.crime.config.DBConnect;
import net.crime.objects.Case;
import net.crime.objects.Criminal;
import net.crime.objects.Police;

import java.text.ParseException;


public class CaseService {
	
	

	public ArrayList<Case> getCaseList() {
         
		ArrayList<Case> caseList=new ArrayList<Case>();
		Connection c=DBConnect.getConnection();		
		Statement stmt = null;
	    String query = "select * from test.case";	             
	    try {
	        stmt = c.createStatement();
	        ResultSet rs = stmt.executeQuery(query);
	        while (rs.next()) {
	        	Case cs=new Case();
	        	cs.setCaseId(rs.getInt(1));
	        	cs.setHappenedOn(rs.getString(2));
	        	cs.setClosedOn(rs.getString(3));
	        	cs.setLocation(rs.getString(4));
	        	cs.setDescription(rs.getString(5));
	        	cs.setStatus(rs.getString(6));
	        	caseList.add(cs);
	        }
	        
	        
	    } catch(Exception e ) {
	        e.printStackTrace();
	    } finally {
	    	try {
	    		   c.close();
	               if (stmt != null) 
				      stmt.close();				      
			         } catch (SQLException e) {
				           e.printStackTrace();
			          } 
	              }
		return caseList;
	}
	
	

	public ArrayList<Police> getAssignedOfficerList(int csId) {
		
		ArrayList<Police> officerList=new ArrayList<Police>();
		Connection c=DBConnect.getConnection();		
		Statement stmt = null;
	    String query = "select policeid, first_name, last_name, dept" +
	    		       " from test.case_assignment NATURAL JOIN test.police"+
                       " where caseId="+csId;	             
	    try {
	        stmt = c.createStatement();
	        ResultSet rs = stmt.executeQuery(query);
	        while (rs.next()) {
	        	Police p=new Police();
	        	p.setPoliceId(rs.getInt("policeid"));
	        	p.setFirstName(rs.getString("first_name"));
	        	p.setLastName(rs.getString("last_name"));
	        	p.setDepartment(rs.getString("dept"));
	        	officerList.add(p);	        	
	        }
	        
	        
	    } catch(Exception e ) {
	        e.printStackTrace();
	    } finally {
	    	try {
	    		   c.close();
	               if (stmt != null) 
				      stmt.close();				      
			         } catch (SQLException e) {
				           e.printStackTrace();
			          } 
	              }
		return officerList;		
	}



	// new create method
	public boolean create(String happenedOn, String closedOn, String location,
			String description, String status) {
		Date date = null;
	    try {	       
			date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(happenedOn);
	    } catch (ParseException ex) {
	       // ex.printStackTrace();
	    }
		if(happenedOn.equalsIgnoreCase("")||location.equalsIgnoreCase("")||description.equalsIgnoreCase("")||status.equalsIgnoreCase("")||date==null||(!status.equalsIgnoreCase("Open")&&!status.equalsIgnoreCase("Close")))
		    return false;
		else
		{
		   Connection c=DBConnect.getConnection();
		   Statement stmt = null;
		   String sql=null;
		   if(closedOn.equalsIgnoreCase(""))
		   {
			   if(status.equalsIgnoreCase("close"))
				   return false;
			   
			    sql = "INSERT INTO test.case(happenedOn, closedOn, location, description, case_status) " +
                     "VALUES ('"+happenedOn+"',"+null+", '"+location+"', '"+description+"', '"+status+"')";
		   }
		   else
		   {
			   date = null;
			    try {
			       // String format;
					date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(closedOn);
			    } catch (ParseException ex) {
			       // ex.printStackTrace();
			    }
			   if(date==null||status.equalsIgnoreCase("Open"))
			   {
				   return false;
				   }
			   else
			   {
			   sql= "INSERT INTO test.case(happenedOn, closedOn, location, description, case_status) " +
	        
					   "VALUES ('"+happenedOn+"', '"+closedOn+"', '"+location+"', '"+description+"', '"+status+"')";
			   }
		   }
		   try {
	           stmt = c.createStatement();
	           stmt.executeUpdate(sql);	        
	        
		   } catch(Exception e ) {
			   e.printStackTrace();
		   } finally {
			       try {
	    		       c.close();
	                   if (stmt != null) 
				          stmt.close();				      
			            } catch (SQLException e) {
				          e.printStackTrace();
			            } 
	                 }
	
		return true;
		}
	}


	public boolean delete(int idSelected) {
		Connection c=DBConnect.getConnection();
		Statement stmt = null;
		String sql = "DELETE FROM test.case where caseid="+idSelected;
		try {
	        stmt = c.createStatement();
	        stmt.executeUpdate(sql);	        
	        
	    } catch(Exception e ) {
	        e.printStackTrace();
	    } finally {
	    	try {
	    		   c.close();
	               if (stmt != null) 
				      stmt.close();				      
			         } catch (SQLException e) {
				           e.printStackTrace();
			          } 
	              }
	
		return true;

	}
	
	public ArrayList<Case> getCases() {
        
		ArrayList<Case> caseList=new ArrayList<Case>();;
		Connection c=DBConnect.getConnection();		
		Statement stmt = null;
	    String query = "select caseid, description from test.case where case_status='Open'";	             
	    try {
	        stmt = c.createStatement();
	        ResultSet rs = stmt.executeQuery(query);
	        while (rs.next()) {
	        	Case cs=new Case();
	        	cs.setCaseId(rs.getInt("caseid"));
	        	cs.setDescription(rs.getString("description"));
	        	caseList.add(cs);
	        }	        
	        
	    } catch(Exception e ) {
	        e.printStackTrace();
	    } finally {
	    	try {
	    		   c.close();
	               if (stmt != null) 
				      stmt.close();				      
			         } catch (SQLException e) {
				           e.printStackTrace();
			          } 
	              }
		return caseList;
	}
	
	
public ArrayList<Police> getPolices() {        
		ArrayList<Police> policeList=new ArrayList<Police>();
		Connection c=DBConnect.getConnection();		
		Statement stmt = null;
	    String query = "select policeid, first_name, last_name, dept from test.police";	             
	    try {
	        stmt = c.createStatement();
	        ResultSet rs = stmt.executeQuery(query);
	        while (rs.next()) {
	        	Police pl=new Police();
	        	pl.setPoliceId(rs.getInt("policeid"));
	        	pl.setFirstName(rs.getString("first_name"));
	        	pl.setLastName(rs.getString("last_name"));
	        	pl.setDepartment(rs.getString("dept"));
	        	policeList.add(pl);
	        }	        
	        
	    } catch(Exception e ) {
	        e.printStackTrace();
	    } finally {
	    	try {
	    		   c.close();
	               if (stmt != null) 
				      stmt.close();				      
			         } catch (SQLException e) {
				           e.printStackTrace();
			          } 
	              }
		return policeList;
	}


// new assign method
public boolean assign(int idSelected, ArrayList<Integer> policeIds) {
	Connection c=DBConnect.getConnection();
	ArrayList<Integer> oldAssignment=new ArrayList<Integer>();
	Statement stmt = null;
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	Date date = new Date();
	String dt=dateFormat.format(date).toString();
	
	try {
		stmt = c.createStatement();
		String sql=null;
		sql="DELETE FROM test.case_assignment where caseid="+idSelected;
		stmt.executeUpdate(sql);
		
		for(int i:policeIds)
		 {
			
		        sql = "INSERT INTO test.case_assignment(caseid, policeid, start_time) " +
               "VALUES ("+idSelected+", "+i+", '"+dt+"')";
               stmt.executeUpdate(sql);	
			
		 }
				
	
		
    } catch(Exception e ) {
        e.printStackTrace();
    } finally {
    	try {
    		   c.close();
               if (stmt != null) 
			      stmt.close();				      
		         } catch (SQLException e) {
			           e.printStackTrace();
		          } 
              }


	return true;
}





public ArrayList<Case> find(int idSelected) {
	ArrayList<Case> caseList=new ArrayList<Case>();
	Connection c=DBConnect.getConnection();		
	Statement stmt = null;
    String query = "select * from test.case where caseid="+idSelected;	             
    try {
        stmt = c.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
        	Case cs=new Case();
        	cs.setCaseId(rs.getInt(1));
        	cs.setHappenedOn(rs.getString(2));
        	cs.setClosedOn(rs.getString(3));
        	cs.setLocation(rs.getString(4));
        	cs.setDescription(rs.getString(5));
        	cs.setStatus(rs.getString(6));
        	caseList.add(cs);
        }
        
        
    } catch(Exception e ) {
        e.printStackTrace();
    } finally {
    	try {
    		   c.close();
               if (stmt != null) 
			      stmt.close();				      
		         } catch (SQLException e) {
			           e.printStackTrace();
		          } 
              }
	return caseList;

	}



// new update method
public boolean update(String happenedOn, String closedOn, String location,
		String description, String status, int idSelected) {
	Date date = null;
    try {	       
		date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(happenedOn);
    } catch (ParseException ex) {
       // ex.printStackTrace();
    }
	if(happenedOn.equalsIgnoreCase("")||location.equalsIgnoreCase("")||description.equalsIgnoreCase("")||status.equalsIgnoreCase("")|| date==null||(!status.equalsIgnoreCase("Open")&&!status.equalsIgnoreCase("Close")))
	    return false;
	else
	{
		System.out.println(happenedOn+", "+closedOn+", "+location+", "+description+", "+status+", "+idSelected);
		Connection c=DBConnect.getConnection();		
		Statement stmt = null;	
		String sql=null;
		if(closedOn.equalsIgnoreCase("")||closedOn.equalsIgnoreCase("NA"))
		{
			if(status.equalsIgnoreCase("close"))
				   return false;
			sql = "UPDATE test.case set happenedon='"+happenedOn+"', closedOn=NULL, location='"+location+
					"', description='"+description+"', case_status='"+status+"' where caseid="+idSelected;
		}
		else
		{
			System.out.println("here");
			date = null;
		    try {
		       // String format;
				date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(closedOn);
		    } catch (ParseException ex) {
		       // ex.printStackTrace();
		    }
		   if(date==null||status.equalsIgnoreCase("Open"))
		   {
			   return false;
			   }
		   else
		   {
		
			sql = "UPDATE test.case set happenedon='"+happenedOn+"', closedOn='"+closedOn+"', location='"+location+
					"', description='"+description+"', case_status='"+status+"' where caseid="+idSelected;
		   }
		}
		
		try {
		   stmt = c.createStatement();
	       stmt.executeUpdate(sql);
		 } catch(Exception e ) {
		        e.printStackTrace();
		    } finally {
		    	try {
		    		   c.close();
		               if (stmt != null) 
					      stmt.close();				      
				         } catch (SQLException e) {
					           e.printStackTrace();
				          } 
		              }

		return true;
	}
	
}



public ArrayList<Criminal> getSuspectList(int csId) {
	
	ArrayList<Criminal> suspectList=new ArrayList<Criminal>();
	Connection c=DBConnect.getConnection();		
	Statement stmt = null;
    String query = "select criminalid, first_name, last_name, description" +
    		       " from test.criminal NATURAL JOIN test.crime"+
                   " where caseId="+csId;	             
    
	
	//String query = "select criminalid, first_name, last_name, description from test.criminal NATURAL JOIN test.crime where caseId=7;";
    		       
    
    		       
    try {
        stmt = c.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
        	Criminal cm=new Criminal();
        	cm.setCriminalId(rs.getInt("criminalid"));
        	cm.setCriminalFirstName(rs.getString("first_name"));
        	cm.setCriminalLastName(rs.getString("last_name"));
        	cm.setCriminalDescription(rs.getString("description"));
        	suspectList.add(cm);	        	
        }
        
        
    } catch(Exception e ) {
        e.printStackTrace();
    } finally {
    	try {
    		   c.close();
               if (stmt != null) 
			      stmt.close();				      
		         } catch (SQLException e) {
			           e.printStackTrace();
		          } 
              }
	return suspectList;		

}



public ArrayList<String> getAllPicture(int idSelected) {
	
	ArrayList<String> picLoc=new ArrayList<String>();
	Connection c=DBConnect.getConnection();		
	Statement stmt = null;
    String query = "select photoid" +
    		       " from test.Photo"+" where caseId="+idSelected;	             
    try {
        stmt = c.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
        	
        	String id=rs.getString("photoid");
        	//String directory=rs.getString("directory");
        	id="http://geolog.engr.uga.edu/crimeDBFiles/testGetImages.php?id="+id;
        	picLoc.add(id);	        	
        }
        
        
    } catch(Exception e ) {
        e.printStackTrace();
    } finally {
    	try {
    		   c.close();
               if (stmt != null) 
			      stmt.close();				      
		         } catch (SQLException e) {
			           e.printStackTrace();
		          } 
              }
	return picLoc;		

}

//Alekhya
public ArrayList<Case> selectInvolvedCases(int crimidselected)
{
	ArrayList<Case> caseList=new ArrayList<Case>();
Connection c=DBConnect.getConnection();		
Statement stmt = null;
String query = "select * from test.case where caseid in (select caseid from test.crime where criminalid= '"+crimidselected+"')";	             
try {
    stmt = c.createStatement();
    ResultSet rs = stmt.executeQuery(query);
    while (rs.next()) {
    	Case cs=new Case();
    	cs.setCaseId(rs.getInt(1));
    	cs.setHappenedOn(rs.getString(2));
    	cs.setClosedOn(rs.getString(3));
    	cs.setLocation(rs.getString(4));
    	cs.setDescription(rs.getString(5));
    	cs.setStatus(rs.getString(6));
    	caseList.add(cs);
    }
    
    
} catch(Exception e ) {
    e.printStackTrace();
} finally {
	try {
		   c.close();
           if (stmt != null) 	
		      stmt.close();				      
	         } catch (SQLException e) {
		           e.printStackTrace();
	          } 
          }
return caseList;
}
//Alekhya
public ArrayList<Integer> selectOtherCases(int crimselected)
{
	ArrayList<Integer> otherCases= new ArrayList<Integer>();
	Connection c=DBConnect.getConnection();		
	Statement stmt = null;
    String query = "select caseid from test.case where caseid not in(select caseid from test.crime where criminalid='"+crimselected+"')";	             
    try {
    	stmt = c.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
        otherCases.add(rs.getInt(1));
        }
    }
    catch(Exception e ) {
        e.printStackTrace();
    } finally {
    	try {
    		   c.close();
               if (stmt != null) 	
			      stmt.close();				      
		         } catch (SQLException e) {
			           e.printStackTrace();
		          } 
              }
	return otherCases;
}

//Alekhya
	public ArrayList<Integer> getCaseIds()
	{
		ArrayList<Integer> ids = new ArrayList<Integer>();
		Connection c=DBConnect.getConnection();		
		Statement stmt = null;
	    String query = "select caseid from test.case";	             
	    try {
	        stmt = c.createStatement();
	        ResultSet rs = stmt.executeQuery(query);
	    while (rs.next()) {
	        	Case cs=new Case();
	        	ids.add(rs.getInt(1));
	         }
	    }
	    catch(Exception e ) {
	        e.printStackTrace();
	    } finally {
	    	try {
	    		   c.close();
	               if (stmt != null) 	
				      stmt.close();				      
			         } catch (SQLException e) {
				           e.printStackTrace();
			          } 
	              }
		return ids;
	        	
	}
	
}
