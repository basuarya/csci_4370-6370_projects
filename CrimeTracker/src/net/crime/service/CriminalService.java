
// Author Alekhya Chennupati

package net.crime.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import net.crime.config.DBConnect;
import net.crime.objects.Criminal;

public class CriminalService {
	
	//public ArrayList<Case> getCaseList() {
	public ArrayList<Criminal> getCriminalList() {
		ArrayList<Criminal> criminalsList=new ArrayList<Criminal>();
		Connection c=DBConnect.getConnection();		
		Statement stmt = null;
	    String query = "select * from test.criminal";	             		
		try {
	        stmt = c.createStatement();
	        ResultSet rs = stmt.executeQuery(query);
	        while (rs.next()) {
	        	Criminal criminals=new Criminal();
	        	criminals.setCriminalId(rs.getInt(1));
	        	criminals.setCriminalFirstName(rs.getString(2));
	        	criminals.setCriminalLastName(rs.getString(3));
	        	criminals.setCriminalGender(rs.getString(4));
	        	criminals.setCriminalDescription(rs.getString(5));
	        	criminalsList.add(criminals);
	        }
	    } catch(Exception e ) {
	        e.printStackTrace();
	    } finally {
	    	try {
	    		   c.close();
	               if (stmt != null) 
				      stmt.close();				      
			         } catch (SQLException e) {
				           e.printStackTrace();
			          } 
	              }
		return criminalsList;
	}
	public String deleteCriminal(int crimId) {
	
		Connection c=DBConnect.getConnection();		
		Statement stmt = null;
	    String query = "delete from test.criminal where criminalid='" +crimId+"'";
	    String status="failure";		       	             
	    try {
	        stmt = c.createStatement();
	       
	        int a=stmt.executeUpdate(query);
	       
	        if (a==1) {
		     System.out.println("Deleted Successfully");
		    status="success";
	        }
	        else
	        	status="failure";
	        }
	        
	        
	     catch(Exception e ) {
	        e.printStackTrace();
	    } finally {
	    	try {
	    		   c.close();
	               if (stmt != null) 
				      stmt.close();				      
			         } catch (SQLException e) {
				           e.printStackTrace();
			          } 
	              }
	    return status;		
	}
	
	public ArrayList<Criminal> editCriminal(int crimId) 
	{
			ArrayList<Criminal> criminalsList=new ArrayList<Criminal>();
			Connection c=DBConnect.getConnection();		
			Statement stmt = null;
		    String query = "select * from test.criminal where criminalid='" +crimId+"'";	             	
			try {
		        stmt = c.createStatement();
		        ResultSet rs = stmt.executeQuery(query);
		        while (rs.next()) {
		        	Criminal criminals=new Criminal();
		        	criminals.setCriminalId(rs.getInt(1));
		        	criminals.setCriminalFirstName(rs.getString(2));
		        	criminals.setCriminalLastName(rs.getString(3));
		        	criminals.setCriminalGender(rs.getString(4));
		        	criminals.setCriminalDescription(rs.getString(5));
		        	criminalsList.add(criminals);
		        }
		    } catch(Exception e ) {
		        e.printStackTrace();
		    } finally {
		    	try {
		    		   c.close();
		               if (stmt != null) 
					      stmt.close();				      
				         } catch (SQLException e) {
					           e.printStackTrace();
				          } 
		              }
			return criminalsList;
		}
	
	public String saveThisCriminal(int criminalIdSelected,String criminalFirstName,String criminalLastName,String criminalGender,String criminalDescription)
	{
		Connection c=DBConnect.getConnection();		
		Statement stmt = null;
	    String query = "update test.criminal set first_name='"+criminalFirstName+"',last_name='"+criminalLastName+"'"
	    				+ ",gender='"+criminalGender+"',description='"+criminalDescription+"' where criminalid='" +criminalIdSelected+"'";
	    String status="failure";		       	             
	    try {
	        stmt = c.createStatement();
	       
	        int a=stmt.executeUpdate(query);
	       
	        if (a==1) {
		     System.out.println("Deleted Successfully");
		    status="success";
	        }
	        else
	        	status="failure";
	        }
	        
	        
	     catch(Exception e ) {
	        e.printStackTrace();
	    } finally {
	    	try {
	    		   c.close();
	               if (stmt != null) 
				      stmt.close();				      
			         } catch (SQLException e) {
				           e.printStackTrace();
			          } 
	              }
	    return status;		
	}
	
	public String insertCriminal(String criminalFirstName,String criminalLastName,String criminalGender,String criminalDescription)
	{
				
		Statement stmt = null;
		String status="failure";
		Connection c=DBConnect.getConnection();
		
		String sql = "INSERT INTO test.criminal(first_name, last_name, gender, description) " +
                     "VALUES ('"+criminalFirstName+"', '"+criminalLastName+"', '"+criminalGender+"', '"+criminalDescription+"')";
		try {
	        stmt = c.createStatement();
	      int stat= stmt.executeUpdate(sql);
	      if(stat==1)
	    	  status="success";
	      else
	    	  status="failure";
	        
	    } catch(Exception e ) {
	        e.printStackTrace();
	     } finally {
	    	try {
	    		   c.close();
	               if (stmt != null) 
				      stmt.close();				      
			         } catch (SQLException e) {
				           e.printStackTrace();
			          } 
	              }
	    
		return status;
	}
	
	public String insertCrime(int cseidSelected)
	{	Connection con=DBConnect.getConnection();		
		Statement stmt = null;
		String status="failure";
		Statement stmt1 = null;
		String sql1 = "select max(criminalid) from test.criminal";
		int crid=0;
		try {
	        stmt1 = con.createStatement();
	        ResultSet rs1 = stmt1.executeQuery(sql1);
	        while (rs1.next())
	        {
	         crid=rs1.getInt(1);
	        }
	        System.out.println("recent criminal id="+crid);
	        System.out.println("cseid="+cseidSelected);
		String sql = "INSERT INTO test.crime(caseid,criminalid) " +
                     "VALUES ('"+cseidSelected+"', '"+crid+"')";
		
	        stmt = con.createStatement();
	      int stat= stmt.executeUpdate(sql);
	      if(stat==1)
	    	  status="success";
	      else
	    	  status="failure";
	        
	    } catch(Exception e ) {
	        e.printStackTrace();
	     } finally {
	    	try {
	    		   con.close();
	              if (stmt1 != null) 
				      stmt1.close();				      
			         } catch (SQLException e) {
				           e.printStackTrace();
			          } 
	              }
		return status;
	}
	
	
	public ArrayList<Criminal> selectAssociatedCriminals(int idSelected)
	{
		ArrayList<Criminal> criminalsAssociated=new ArrayList<Criminal>();
		Connection c=DBConnect.getConnection();		
		Statement stmt = null;
	    String query = "select * from test.criminal where criminalid in (select criminalid from test.crime where caseid= '"+idSelected+"')";
	    String status="failure";		       	             
	    try {
	        stmt = c.createStatement();
	        ResultSet rs = stmt.executeQuery(query);
	        while (rs.next()) {
	        	Criminal criminals=new Criminal();
	        	criminals.setCriminalId(rs.getInt(1));
	        	criminals.setCriminalFirstName(rs.getString(2));
	        	criminals.setCriminalLastName(rs.getString(3));
	        	criminals.setCriminalGender(rs.getString(4));
	        	criminals.setCriminalDescription(rs.getString(5));
	        	criminalsAssociated.add(criminals);
	        }
	    } catch(Exception e ) {
	        e.printStackTrace();
	    } finally {
	    	try {
	    		   c.close();
	               if (stmt != null) 
				      stmt.close();				      
			         } catch (SQLException e) {
				           e.printStackTrace();
			          } 
	              }
		return criminalsAssociated;		
	}
	
	public String insertToCrime(ArrayList<Integer> othrCaseIds,int crimIdSelected)
	{
		Connection con=DBConnect.getConnection();		
		Statement stmt = null;
		String status="failure";
		int stat;
		String[] sql = new String[othrCaseIds.size()];
		
		int[] ret = new int[othrCaseIds.size()];
	    	try
			{
	    		for (int i=0; i < ret.length; i++)
	    	    {
	    	        ret[i] = othrCaseIds.get(i).intValue();
	    	       sql[i]  = "INSERT INTO test.crime(caseid,criminalid) " +
	    	                "VALUES ('"+ret[i]+"', '"+crimIdSelected+"')";
	    	       stmt = con.createStatement();
	    	       stat= stmt.executeUpdate(sql[i]);
	    	       if(stat==1)
	    	    	   status="success";
	    	       else
	    	    	   status="failure";
	    	    }
			}
	    catch(Exception e ) {
	       e.printStackTrace();
	    } finally {
	   	try {
	   		   con.close();
	             if (stmt != null) 
				      stmt.close();				      
			         } catch (SQLException e) {
				           e.printStackTrace();
			          } 
	             }
		return status;
	}
	
	public String deleteCrimfromCrime(int idSelected,int criminalIdSelected)
	{
		Connection c=DBConnect.getConnection();		
		Statement stmt = null;
	    String query = "delete from test.crime where criminalid='" +criminalIdSelected+"'"+"and caseid='"+idSelected+"'";
	    String status="failure";		       	             
	    try {
	        stmt = c.createStatement();
	       
	        int a=stmt.executeUpdate(query);
	       
	        if (a==1) {
		     System.out.println("Deleted Successfully");
		    status="success";
	        }
	        else
	        	status="failure";
	        }
	        
	        
	     catch(Exception e ) {
	        e.printStackTrace();
	    } finally {
	    	try {
	    		   c.close();
	               if (stmt != null) 
				      stmt.close();				      
			         } catch (SQLException e) {
				           e.printStackTrace();
			          } 
	              }
	    return status;		
	}
}
