package net.crime.objects;

public class Specialization {
	private int sid;
	private String sName;
	private String sDesc;
	
	public Specialization() {
		
	}
	
	public Specialization(int sid, String sName, String sDesc) {
		this.sid = sid;
		this.sName = sName;
		this.sDesc = sDesc;
	}
	
	public int getSid (){
		return sid;
	}
	
	public void setSid (int sid) {
		this.sid = sid;
	}
	
	public String getSName() {
		return this.sName;
	}
	
	public void setSName(String sName) {
		this.sName = sName;
	}
	
	public String getSDesc() {
		return this.sDesc;
	}
	
	public void setSDesc(String sDesc) {
		this.sDesc = sDesc;
	}
	
	public String toString() {
		return this.sName;
	}

}
