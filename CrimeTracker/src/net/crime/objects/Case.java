package net.crime.objects;

public class Case {
	
	private int caseId;
	private String happenedOn;
	private String closedOn;
	private String location;
	private String description;
	private String status;
	
	public int getCaseId() {
		
		return caseId;
	}
	public void setCaseId(int caseId) {
		this.caseId = caseId;
	}
	public String getHappenedOn() {
		return happenedOn;
	}
	public void setHappenedOn(String happenedOn) {
		this.happenedOn = happenedOn;
	}
	public String getClosedOn() {
		return closedOn;
	}
	public void setClosedOn(String closedOn) {
		if(closedOn!=null)
		    this.closedOn = closedOn;
		else
			this.closedOn ="NA";
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		if(location!=null)
		   this.location = location;
		else
			this.location = "UNKNOWN";
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
		

}
