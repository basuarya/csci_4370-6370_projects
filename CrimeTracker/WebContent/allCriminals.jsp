<!-- Alekhya Chennupati  -->

<jsp:include page="header.jsp" />
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Criminals List</title>
</head>
<body background="http://geolog.engr.uga.edu/crimeDBFiles/backGround-1.jpg">
<h1>All Criminals</h1>
  
<table border="1">
<tr>
    <th>Criminal ID</th>
    <th>First Name</th>
    <th>Last Name</th>
    <th>Gender</th>
    <th>Criminal Description</th>
    </tr>
<s:iterator value="criminalList">

    <tr>

<td><s:property value="criminalId"/></td> 
<td><s:property value="criminalFirstName"/></td> 
<td><s:property value="criminalLastName"/></td>  
<td><s:property value="criminalGender"/></td>  
<td><s:property value="criminalDescription"/></td> 
<td><a href="addOrShowcases?crimIdSelected=<s:property value="criminalId"/>">Add/View Cases Involved</a></td>
<td><a href="editCriminal?criminalIdSelected=<s:property value="criminalId"/>">Edit Criminal</a></td>
<td><a href="deleteCriminal?criminalIdSelected=<s:property value="criminalId"/>">Delete Criminal</a></td>

       </tr> 
 
</s:iterator>

</table><br/>




</body>
</html>