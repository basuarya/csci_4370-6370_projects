
<jsp:include page="header.jsp" />
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body background="http://geolog.engr.uga.edu/crimeDBFiles/backGround-1.jpg">
<h1>Assigned Officers</h1>
<p>Case ID: <s:property value="idSelected"/></p>

<table border="1">
<tr>
    <th>PoliceID</th>
    <th>Name</th>
    <th>Department</th>
    
 </tr>
<s:iterator value="officerList">

    <tr>

<td><s:property value="policeId"/></td> 
<td><s:property value="firstName"/>, <s:property value="lastName"/></td>
<td><s:property value="department"/></td>  

       </tr> 
 
</s:iterator>

</table><br/>
<h1>Suspects</h1>
<table border="1">
<tr>
    <th>CriminalID</th>
    <th>Name</th>
    <th>Description</th>
    
 </tr>
<s:iterator value="suspectList">

    <tr>

<td><s:property value="criminalId"/></td> 
<td><s:property value="criminalFirstName"/>, <s:property value="criminalLastName"/></td>
<td><s:property value="criminalDescription"/></td>  

       </tr> 
 
</s:iterator>

</table><br/>
</body>
</html>