<jsp:include page="header.jsp" />
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Case Details</title>
</head>
<body background="http://geolog.engr.uga.edu/crimeDBFiles/backGround-1.jpg">

<h1>Case Details</h1>
  
<table border="1">
<tr>
    <th>CaseID</th>
    <th>HappenedOn</th>
    <th>ClosedOn</th>
    <th>Location</th>
    <th>Description</th>
    <th>Status</th>
    <th>Officers</th>
    <th>Photos</th>
    <th>Edit/Delete</th>
 </tr>
<s:iterator value="caseList">

    <tr>

<td><s:property value="caseId"/></td> 
<td><s:property value="happenedOn"/></td> 
<td><s:property value="closedOn"/></td>  
<td><s:property value="location"/></td>  
<td><s:property value="description"/></td>  
<td><s:property value="status"/></td>
<td><a href="getAssignment?idSelected=<s:property value="caseId"/>">Assignments</a></td>  
<td><a href="getPics?idSelected=<s:property value="caseId"/>">Pics</a></td> 
<td><a href="deleteCase?idSelected=<s:property value="caseId"/>">Delete</a>/<a href="findCase?idSelected=<s:property value="caseId"/>">Edit</a></td> 

       </tr> 
 
</s:iterator>

</table><br/>




</body>
</html>