Bhargabi Chakrabarti
====================

Manager Evaluation:
-------------------

Our manager did an excellent job. He deserves a +2.


My Responsibilities:
---------------------


iSelect()- For indexed select. Gets keys generated from the index. selects all the 
tuples for the keys retrieved using the get method of the index structures.

rangeSelect() -For indexed range select. Gets the first and last key for the B+ Tree.
Retrieves the submap of the B+ tree, gets all tuples from the submap.

rangeSelectNormal() - For normal range select without index. Gets first and last key 
from B+tree. Gets all the tuples from the attribute tuples of Table class, which falls
between first and last key.

StudentDB.java - Wrote code to call the Tuplegenerator and TestTupleGenerator.For inserting
the tuples in the respective tables. The time functions to calculate and display the time.

