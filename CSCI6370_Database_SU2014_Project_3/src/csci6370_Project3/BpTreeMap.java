package csci6370_Project3;


/************************************************************************************
 * @file BpTreeMap.java
 *
 * @author  John Miller
 */

import java.io.*;
import java.lang.reflect.Array;

import static java.lang.System.out;

import java.util.*;
import java.util.Map.Entry;

/************************************************************************************
 * This class provides B+Tree maps.  B+Trees are used as multi-level index structures
 * that provide efficient access for both point queries and range queries.
 * This is the one for Project 3.
 */
public class BpTreeMap <K extends Comparable <K>, V>
       extends AbstractMap <K, V>
       implements Serializable, Cloneable, SortedMap <K, V>
{
    /** The maximum fanout for a B+Tree node.
     */
    private static final int ORDER = 5;

    /** The class for type K.
     */
    private final Class <K> classK;

    /** The class for type V.
     */
    private final Class <V> classV;

    /********************************************************************************
     * This inner class defines nodes that are stored in the B+tree map.
     */
    private class Node
    {
        boolean   isLeaf;
        int       nKeys;
        K []      key;
        Object [] ref;
        @SuppressWarnings("unchecked")
        Node (boolean _isLeaf)
        {
            isLeaf = _isLeaf;
            nKeys  = 0;
            key    = (K []) Array.newInstance (classK, ORDER - 1);
            if (isLeaf) {
                //ref = (V []) Array.newInstance (classV, ORDER);
                ref = new Object [ORDER];
            } else {
                ref = (Node []) Array.newInstance (Node.class, ORDER);
            } // if
        } // constructor
    } // Node inner class

    /** The root of the B+Tree
     */
    private final Node root;
//private Node root;
    /** The counter for the number nodes accessed (for performance testing).
     */
    private int count = 0;

    /********************************************************************************
     * Construct an empty B+Tree map.
     * @param _classK  the class for keys (K)
     * @param _classV  the class for values (V)
     */
    public BpTreeMap (Class <K> _classK, Class <V> _classV)
    {
        classK = _classK;
        classV = _classV;
        root   = new Node (true);
    } // constructor

    /********************************************************************************
     * Return null to use the natural order based on the key type.  This requires the
     * key type to implement Comparable.
     */
    public Comparator <? super K> comparator () 
    {
        return null;
    } // comparator

    /********************************************************************************
     * Return a set containing all the entries as pairs of keys and values.
     * @return  the set view of the map
     */
    public Set <Map.Entry <K, V>> entrySet ()
    {
    	 Set <Map.Entry <K, V>> enSet = new HashSet <> ();
         for(int i=0; i<root.ref.length; ++i){
         enSet = entrySet(enSet,(BpTreeMap<K,V>.Node)root.ref[i]);
         }
         out.println("inside entrySet function");
 		for(Map.Entry<K, V> entry:enSet){
 		out.print(entry.getKey() + " ");
 		}
 		return enSet;
    } // entrySet
    
    /********************************************************************************
     * Return a set containing all the entries as pairs of keys and values.
     * @param n Node to start search
     * @param enSet set to append to
     * @return   the resulting set
     */    
    private Set <Map.Entry <K,V>> entrySet(Set<Map.Entry<K, V>> enSet, Node n){
    if(n != null){
    if(n.isLeaf)
    {
        for(int i=0; i<n.nKeys; ++i){
        Map.Entry<K,V> temp = new AbstractMap.SimpleEntry(n.key[i], n.ref[i]);
        enSet.add(temp);
        }// for
    }// if
    else
    {
    for(int i = 0; i<n.ref.length; ++i)
    {
    enSet = entrySet(enSet, (BpTreeMap<K,V>.Node)n.ref[i] );
    } // for
    }// else
    }// if
    return enSet;
    }


    /********************************************************************************
     * Given the key, look up the value in the B+Tree map.
     * @param key  the key used for look up
     * @return  the value associated with the key
     */
    @SuppressWarnings("unchecked")
    public V get (Object key)
    {
        return find ((K) key, root);
    } // get

    /********************************************************************************
     * Put the key-value pair in the B+Tree map.
     * @param key    the key to insert
     * @param value  the value to insert
     * @return  null (not the previous value)
     */
    public V put (K key, V value)
    {
    	Node locationNode = root;
    	boolean checkInsert=false;
    	Node parentNode = null;
    	K k_i;
    	if(root.isLeaf)    		insert (key, value, root, null);
    	else{
    		 outerLoop: {
    			do{
    				for (int i = 0; i < locationNode.nKeys; i++) 
		    		{
		    			k_i = locationNode.key [i];
		    			//out.println("k_i="+k_i);
		    			if (key.compareTo (k_i) < 0 && !checkInsert) 
		    			{
		                	if(!locationNode.isLeaf) locationNode= (Node) locationNode.ref[i];
		                	else{
		                		checkInsert=true;
		                		//insert(key,value,locationNode,parentNode);
		                		break outerLoop;
		                	}
		                } 
		                else if (key.equals (k_i) )		out.println ("BpTreeMap:insert: attempt to insert duplicate key = " + key);
		                // if
		            }
    				if (locationNode.isLeaf){
    					checkInsert=true;
    					//insert(key,value,locationNode,parentNode);
    					break outerLoop;
    				}
    				parentNode = locationNode;
    				if(!checkInsert)	 locationNode = (Node) locationNode.ref[locationNode.nKeys];
    			}
    			while(!checkInsert);
    		}
    		insert(key,value,locationNode,parentNode);
    		}
	     	
	     	// for
    	//} 
        return null;
    } // put

    /********************************************************************************
     * Return the first (smallest) key in the B+Tree map.
     * @return  the first key in the B+Tree map.
     */
    public K firstKey () 
    {
    	 K smallest;
    	 Node leastNode = (Node) root.ref[0];
    	if(root.isLeaf)smallest = root.key[0];
    	else 
    	{
    		do
    		{
    			if(!leastNode.isLeaf)
    			leastNode = (Node) leastNode.ref[0];
    			
    		}
    		while(!leastNode.isLeaf);
    		smallest= leastNode.key[0];	
    	}
    	return smallest;
        //  T O   B E   I M P L E M E N T E D
    	
    	
    	
    	
    	
      //  return null;
    } // firstKey

    /********************************************************************************
     * Return the last (largest) key in the B+Tree map.
     * @return  the last key in the B+Tree map.
     */
    public K lastKey () 
    {
    	K largest;
    	Node largestNode = (Node) root.ref[root.nKeys];
	   	if(root.isLeaf)largest = root.key[root.nKeys-1];
	   	else 
	   	{
	   		do
	   		{
	   			if(!largestNode.isLeaf)
	   				largestNode = (Node) largestNode.ref[largestNode.nKeys];
	   			
	   		}
	   		while(!largestNode.isLeaf);
	   		largest= largestNode.key[largestNode.nKeys-1];	
	   	}
	   	return largest;
       // return null;
    } // lastKey

    /********************************************************************************
     * Return the portion of the B+Tree map where key < toKey.
     * @return  the submap with keys in the range [firstKey, toKey)
     */
    public SortedMap <K,V> headMap (K toKey)
    {
     
    	 Set<Map.Entry<K,V>> all = entrySet();
    	    SortedMap <K,V> fin = new TreeMap<K,V>();
    	   
    	        //  T O   B E   I M P L E M E N T E D
    	Iterator<Entry<K,V>> it = all.iterator();
    	while (it.hasNext()) {
    	Map.Entry<K,V> temp = it.next();
    	K key = temp.getKey();
    	V val = temp.getValue();
    	if (key.compareTo(toKey) < 0){
    	fin.put(key, val);
    	} // if
    	} // while
    	 
    	 return fin;
 
    } // headMap

    /********************************************************************************
     * Return the portion of the B+Tree map where fromKey <= key.
     * @return  the submap with keys in the range [fromKey, lastKey]
     */
    public SortedMap <K,V> tailMap (K fromKey)
    {
    	 Set<Map.Entry<K,V>> all = entrySet();
    	    SortedMap <K,V> fin = new TreeMap<K,V>();
    	   
    	        //  T O   B E   I M P L E M E N T E D
    	Iterator<Entry<K,V>> it = all.iterator();
    	while (it.hasNext()) {
    	Map.Entry<K,V> temp = it.next();
    	K key = temp.getKey();
    	V val = temp.getValue();
    	if (key.compareTo(fromKey) >= 0){
    	fin.put(key, val);
    	} // if
    	} // while
    	 
    	return fin;

    } // tailMap

    /********************************************************************************
     * Return the portion of the B+Tree map whose keys are between fromKey and toKey,
     * i.e., fromKey <= key < toKey.
     * @return  the submap with keys in the range [fromKey, toKey)
     */
    public SortedMap <K,V> subMap (K fromKey, K toKey)
    {
    	  Set<Map.Entry<K,V>> all = entrySet();
    	    SortedMap <K,V> fin = new TreeMap<K,V>();
    	   
    	        //  T O   B E   I M P L E M E N T E D
    	Iterator<Entry<K,V>> it = all.iterator();
    	while (it.hasNext()) {
    	Map.Entry<K,V> temp = it.next();
    	K key = temp.getKey();
    	V val = temp.getValue();
    	if (key.compareTo(fromKey) >= 0 && key.compareTo(toKey) < 0){
    	fin.put(key, val);
    	} // if
    	} // while
    	 
    	 return fin;

    } // subMap

    /********************************************************************************
     * Return the size (number of keys) in the B+Tree.
     * @return  the size of the B+Tree
     */
    public int size ()
    {
    	 int size = 0;
         if(root.isLeaf)
         size = root.nKeys;
         else
         for(int i=0; i<root.ref.length; ++i)
         {
         size += size((BpTreeMap<K,V>.Node)root.ref[i]);
         }
         return size;
    } // size
    
    /********************************************************************************
     * Recursive helper function for size
     * @param n the current node of tree
     */
    private int size(BpTreeMap<K,V>.Node node)
    {
    int size = 0;
    if(node != null){
    if(!node.isLeaf){
    for(int i=0; i<node.ref.length; ++i)
    size += size((Node)node.ref[i]);
    } // if
    else size = node.nKeys;
    }
    return size;
    }// size

    /********************************************************************************
     * Print the B+Tree using a pre-order traveral and indenting each level.
     * @param n      the current node to print
     * @param level  the current level of the B+Tree
     */
    @SuppressWarnings("unchecked")
    public void print (Node n, int level)
    {
        out.println ("BpTreeMap");
        out.println ("-------------------------------------------");

        for (int j = 0; j < level; j++) out.print ("\t");
        out.print ("[ . ");
        
        for (int i = 0; i < n.nKeys; i++)
        
        	out.print (n.key [i] + " . ");
        
        out.println ("]");
        if ( ! n.isLeaf) {
            for (int i = 0; i <= n.nKeys; i++) print ((Node) n.ref [i], level + 1);
        } // if

        out.println ("-------------------------------------------");
    } // print

    /********************************************************************************
     * Recursive helper function for finding a key in B+trees.
     * @param key  the key to find
     * @param ney  the current node
     */
    @SuppressWarnings("unchecked")
    private V find (K key, Node n)
    {
        count++;
        for (int i = 0; i < n.nKeys; i++) {
            K k_i = n.key [i];
            if (key.compareTo (k_i) <= 0) {
                if (n.isLeaf) {
                    return (key.equals (k_i)) ? (V) n.ref [i] : null;
                } else {
                    return find (key, (Node) n.ref [i]);
                } // if
            } // if
        } // for
        return (n.isLeaf) ? null : find (key, (Node) n.ref [n.nKeys]);
    } // find

    /********************************************************************************
     * Recursive helper function for inserting a key in B+trees.
     * @param key  the key to insert
     * @param ref  the value/node to insert
     * @param n    the current node
     * @param p    the parent node
     * @author Alekhya Chennupati
     */
    private void insert (K key, V ref, Node n, Node p)
    {  

    	if (n.nKeys < ORDER - 1) {
    		for (int i = 0; i < n.nKeys; i++) 
            {
                K k_i = n.key [i];
                if (key.compareTo (k_i) < 0) 
                {
                	out.println("insert less than");
                    wedge (key, ref, n, i);
                    break;
                } 
                else if (key.equals (k_i)) 
                {
                    out.println ("BpTreeMap:insert: attempt to insert duplicate key = " + key);
                } // if
            } // for

    		wedge (key, ref, n, n.nKeys);
        } else {
        	Node sib = split (key, ref, n);
            if(p==null)
            {
            	Node leftNode = new Node(true);
	            for(int i=0;i<ORDER/2;i++)
	            {
	            	leftNode.key[i]=n.key[i];
	            	leftNode.ref[i]=n.ref[i];
	            	leftNode.nKeys++;
	            	n.key[i]=null;
	            	n.nKeys--;
	            }
	            n.isLeaf = false;
	            n.key[0]  = sib.key[0]; 
	            n.nKeys++;
	            n.ref[0]=leftNode;
	            n.ref[1]=sib;
	            if (n.nKeys < ORDER - 1) 
	           	{
	                for (int i = 0; i < n.nKeys; i++)
	                {
	                    K k_i = n.key [i];
	                    if (key.compareTo (k_i) < 0) 		insert(key,ref,leftNode,n);
	                    else if (key.compareTo(k_i)>=0)		insert(key,ref,sib,n);
	                }
	           	}
	        }
            else{
            	 if(sib.isLeaf){
	            	 insert(sib.key[0],(V)sib,p,getParentNode(p));
		        	 if(sib.key[0].compareTo(key)<0) insert(key,ref,sib,p);
		        	 else                            insert(key,ref,n,p);
		        	 wedgeRef(n.key[0],n,p);
		        }else 
            	 {
            		 Node leftNonLeafNode = new Node(false);
            		 for(int i=0;i<ORDER/2;i++)
            		 {
            			 leftNonLeafNode.key[i] = n.key[i];
            			 leftNonLeafNode.ref[i] = n.ref[i];
            			 leftNonLeafNode.nKeys++;
            			 n.key[i]=null;
            			 n.nKeys--;
            		 }
            		 leftNonLeafNode.ref[leftNonLeafNode.nKeys]=sib.ref[0];
            		 n.key[0] = sib.key[0];
            		 n.nKeys++;
            		 n.ref[0]=leftNonLeafNode;
            		 n.ref[1]=sib;
            		 for(int i=0;i<sib.nKeys;i++)
            		 {
            			 sib.key[i]=sib.key[i+1];
            			 sib.ref[i] = sib.ref[i+1];
            		 }
            		 sib.nKeys--;
            		 if (n.nKeys < ORDER - 1) 
     	           	 {
     	                for (int i = 0; i < n.nKeys; i++)
     	                {
     	                    K k_i = n.key [i];
     	                    if (key.compareTo (k_i) < 0) 		insert(key,ref,leftNonLeafNode,n);
     	                    else if (key.compareTo(k_i)>=0)		insert(key,ref,sib,n);
     	                }
     	           	}
            		 Node temp = (Node) sib.ref[sib.nKeys-1];
            		 sib.ref[sib.nKeys-1] = sib.ref[sib.nKeys];
            		 sib.ref[sib.nKeys] = temp;
                   }
	       }
        } 
    } // insert
    
    /********************************************************************************
     * Wedge only ref value after split.
     * @param key  the key to insert
     * @param currentNode    the current node
     * @param p    the parent node
     * @author Alekhya Chennupati
     * 
     * This method pushes reference into a node. This is helpful to push stranded references on to a Node.
     */
    
    
    private void wedgeRef(K key,Node currentNode,Node p)
    {
    	for (int i = 0; i < p.nKeys; i++) 
		{
    		if(key.compareTo(p.key[i])==0){
    			for (int j = p.nKeys; j > i; j--) 
    			{
    				p.ref[j] = p.ref[j-1];
    			}
    			p.ref[i+1]=currentNode;
    		}
		}
    }
    
    
    /********************************************************************************
     * Get Parent helper function for retrieving parent of a node in B+trees.
     * @param p    the parent node
     * @author Alekhya Chennupati
     *
     */
    private Node getParentNode(Node p){
    	if(p==root){
    		//out.println("Returning Null");
    		//return null;
    	}
    	Node locationNode=root;
    	boolean checkInsert=false;
    	int position = 0;
    	K k_i;
    		 outerLoop: {
    			do{
    				for (int i = 0; i < locationNode.nKeys; i++) 
		    		{
		    			k_i = locationNode.key [i];
		    			if (p.key[0].compareTo (k_i) == 0) break outerLoop;
		    			else if(p.key[0].compareTo (k_i) < 0){
		    				if(((Node)  locationNode.ref[i]).isLeaf)  break outerLoop;
		    				else		locationNode  = (Node) locationNode.ref[i];
		    			}
		            }
    				if(!checkInsert){
    					if(((Node)  locationNode.ref[locationNode.nKeys]).isLeaf)  break outerLoop;
	    				else		locationNode  = (Node) locationNode.ref[locationNode.nKeys];
    				}
    			}
    			while(!checkInsert);
    	  } 
        return locationNode;
    }
   

    /********************************************************************************
     * Wedge the key-ref pair into node n.
     * @param key  the key to insert
     * @param ref  the value/node to insert
     * @param n    the current node
     * @param i    the insertion position within node n
     */
    private void wedge (K key, V ref, Node n, int i)
    {
        for (int j = n.nKeys; j > i; j--) 
        {
            n.key [j] = n.key [j - 1];
            n.ref [j] = n.ref [j - 1];
        } // for
        n.key [i] = key;
        n.ref [i] = ref;
        n.nKeys++;
    } // wedge

    /********************************************************************************
     * Split node n and return the newly created node.
     * @param key  the key to insert
     * @param ref  the value/node to insert
     * @param n    the current node
     */
    private Node split (K key, V ref, Node n)
    {    	
    	Node newNode;
    	if(n.isLeaf) {  	newNode= new Node(true);
    	 //else
           //	newNode=new Node(false);
        
	        for(int i=0,j=ORDER/2;j<ORDER-1;j++,i++)
	        {
        		newNode.key[i]=n.key[j];
        		newNode.ref[i]=n.ref[j];
        		n.key[j]=null;
        		n.ref[j]=null;
        		newNode.nKeys++;
        		n.nKeys--;
	     
	        }
	        //newNode.ref[newNode.nKeys]= n.ref[ORDER-1];
    	}
        else
        {
        	newNode=new Node(false);
        	for(int i=0,j=ORDER/2;j<ORDER-1;j++,i++)
            {
            		newNode.key[i]=n.key[j];
            		newNode.ref[i]=n.ref[j];
            		n.key[j]=null;
            		n.ref[j]=null;
            		newNode.nKeys++;
            		n.nKeys--;
            }
        	newNode.ref[newNode.nKeys]= n.ref[ORDER-1];
         }
          return newNode;
      } // split

    /********************************************************************************
     * The main method used for testing.
     * @param  the command-line arguments (args [0] gives number of keys to insert)
     */
    public static void main (String [] args)
    {
        BpTreeMap <Integer, Integer> bpt = new BpTreeMap <> (Integer.class, Integer.class);
        int totKeys = 10;
       // out.println("HEYYYY"+args[0]+"length="+args.length);
        if (args.length == 1)	totKeys = Integer.valueOf (args [0]);
        for (int i = 1; i < totKeys; i += 2) bpt.put (i, i * i);
        bpt.print (bpt.root, 0);
       // out.println("the smallest key is"+bpt.firstKey()); 
        //out.println("the largest key is"+bpt.lastKey());
        for (int i = 0; i < totKeys; i++) {
            out.println ("key = " + i + " value = " + bpt.get (i));
        } // for
        out.println ("-------------------------------------------");
        out.println ("Average number of nodes accessed = " + bpt.count / (double) totKeys);
        
        
        System.out.println("size " + bpt.size());
        
        Set <Map.Entry <Integer, Integer>> enSet = new HashSet <> ();
        enSet = bpt.entrySet();
		Iterator it = enSet.iterator();
		out.println("-------------------------------------------");
		out.println("All entrySet - test entrySet function");
		while(it.hasNext()) {
		out.println(it.next());
		}
		
		out.println("\n ......... headMap ...........");
		SortedMap  <Integer, Integer> headmap = new TreeMap<>();
		headmap = bpt.headMap((Integer)6);
		out.println("submap with key values less than 6");
		for(Map.Entry<Integer, Integer> entry:headmap.entrySet()){
		out.print(entry.getKey() + " ");
		}
		 
		out.println("\n ......... tailMap ............");
		SortedMap  <Integer, Integer> tailmap = new TreeMap<>();
		tailmap = bpt.tailMap((Integer)6);
		out.println("submap with key values greater than 6");
		for(Map.Entry<Integer, Integer> entry:tailmap.entrySet()){
		out.print(entry.getKey() + " ");
		}
		 
		out.println("\n ......... subMap ............");
		SortedMap  <Integer, Integer> submap = new TreeMap<>();
		submap = bpt.subMap((Integer)2,(Integer)6);
		out.println("submap with key values between 2 and 6");
		for(Map.Entry<Integer, Integer> entry:submap.entrySet()){
		out.print(entry.getKey() + " ");
		}
		 
		System.out.println("\n .......Key Tests.......");
		System.out.println("first key = " + bpt.firstKey());
		System.out.println("last key = " + bpt.lastKey());
		 
		
       

    } // main

} // BpTreeMap class






