package csci6370_Project3;
import static java.lang.System.out;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.Random;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;


/*****************************************************************************************
 * The StudentDB class makes a Student Database. 
 * @author Bhargabi Chakrabarti
 */
class StudentDB implements Serializable {
	/*************************************************************************************
	 * Main method for creating, populating and querying a Student Database.
	 * 
	 * @param args
	 *            the command-line arguments
	 */
	public static void main(String[] args) {
		double start = 0.0, timeTaken;
		Random rand = new Random ();
		
		//out.println();

		TupleGenerator tupleGen=new TupleGeneratorImpl();
		
		//creating tables
		Table student = new Table ("Student", "id name address status",
				"Integer String String String", "id");

		Table professor = new Table ("Professor", "id name deptId",
					"Integer String String", "id");

		Table course = new Table ("Course", "crsCode deptId crsName descr",
				"String String String String", "crsCode");

		Table teaching = new Table ("Teaching", "crsCode semester profId",
					"String String Integer", "crsCode semester");

		Table transcript = new Table ("Transcript", "studId crsCode semester grade",
					"Integer String String String", "studId crsCode semester");

		//adding schemas to TupleGenerator
		tupleGen.addRelSchema ("Student",
                "id name address status",
                "Integer String String String",
                "id",
                null);

		tupleGen.addRelSchema ("Professor",
                "id name deptId",
                "Integer String String",
                "id",
                null);

		tupleGen.addRelSchema ("Course",
                "crsCode deptId crsName descr",
                "String String String String",
                "crsCode",
                null);

		tupleGen.addRelSchema ("Teaching",
                "crsCode semester profId",
                "String String Integer",
                "crcCode semester",
                new String [][] {{ "profId", "Professor", "id" },
                                 { "crsCode", "Course", "crsCode" }});

		tupleGen.addRelSchema ("Transcript",
                "studId crsCode semester grade",
                "Integer String String String",
                "studId crsCode semester",
                new String [][] {{ "studId", "Student", "id"},
                                 { "crsCode", "Course", "crsCode" },
                                 { "crsCode semester", "Teaching", "crsCode semester" }});
		
		
		
		// here for the sake of Arya's Linear hashing, I have made the SLOTS variable static to be re-used here.
		// Guannan's Note: I wonder why we hardcoded the number of student tuples?
		//                 I commented the LinHashMap.SLOTS line out, check it please.
		//int tups [] = new int [] {LinHashMap.SLOTS, 6, 4, 4, 6 };
		
		int student_row_num = 2000;   // we should test a different number - GW
		int transcript_row_num = 1000; // we should test a different number - GW
		int tups [] = new int [] {student_row_num, 6, 4, 4, transcript_row_num };		
	    
		
        Comparable [][][] resultTest = tupleGen.generate (tups);
        
        for (int i = 0; i < resultTest.length; i++) {
           // out.println (tables [i]);
            for (int j = 0; j < resultTest [i].length; j++) {
            	Comparable[] tup=new Comparable[resultTest[i][j].length];
                for (int k = 0; k < resultTest [i][j].length; k++) {
                	tup[k]=resultTest [i][j][k];
                	                    
                } // for
                
                // code to be appended here
                
                if(i==0)  // student table
                {
                	// conditional for index type; check indextype in Table.java - added by Arya
                	// retrying the same insert method, so no more conditions                	
                	student.insert2(tup);
                } else if(i == 4) { // transcript table
                	transcript.insert2(tup);
                }
                //out.println ();
            } // for
            //out.println ();
            
            
        } // for
        
       
      //  student.printIndex();
       
     /*Select performance testing*/      
        
//        
//        start=System.currentTimeMillis();
//        Table t_select=student.select ();
//        timeTaken=System.currentTimeMillis() - start;
//        System.out.println("Time for select: "+timeTaken+" ms");
//        System.out.println("Writing the result it to a file");
//        try {
//			 
//			String name = "/OutputSelect"+Table.indexType+".txt";
//        	String content = "Time for select: "+timeTaken+" ms";
//			File file = new File(Table.DIR+name);
// 
//			// if file doesnt exists, then create it
//			if (!file.exists()) {
//				file.createNewFile();
//			}
// 
//			FileWriter fw = new FileWriter(file.getAbsoluteFile());
//			BufferedWriter bw = new BufferedWriter(fw);
//			bw.write(content);
//			bw.close();
// 
//			System.out.println("Done");
// 
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//        
//        //t_select.print();
//        
//      
//       
//        
//        /*Indexed select (arya) performance testing*/      
//        // another approach to indexed select- only to be used later! NOT IN USAGE NOW!
//        if(Table.indexType == "LinHashMap") {
//	        start=System.currentTimeMillis();
//	        Table t_iAryaSelect=student.iAryaSelect();
//	        
//	        timeTaken=System.currentTimeMillis() - start;
//	        System.out.println("Time for indexed select (arya): "+timeTaken+" ms");
//	        System.out.println("This version of linear hashing is an experimental build.");
//	        System.out.println("This is a LinearHashMap implementation using Directories, just like extendible hashing.\nRef: Database Management Systems; Ramakrishnan, Gehrke.\nThat's why the low performance.");
//	        
//	        //t_iAryaSelect.print();
//        } else {
//        	 /*Indexed select performance testing*/      
//            
//            
//            start=System.currentTimeMillis();
//            Table t_iselect=student.iSelect ();
//            timeTaken=System.currentTimeMillis() - start;
//            System.out.println("Time for indexed select: "+timeTaken+" ms");
//            
//            //t_iselect.print();
//            
//            System.out.println("Writing the result it to a file");
//            try {
//    			 
//    			String name = "/OutputIndexedSelect"+Table.indexType+".txt";
//            	String content = "Time for select: "+timeTaken+" ms";
//    			File file = new File(Table.DIR+name);
//     
//    			// if file doesnt exists, then create it
//    			if (!file.exists()) {
//    				file.createNewFile();
//    			}
//     
//    			FileWriter fw = new FileWriter(file.getAbsoluteFile());
//    			BufferedWriter bw = new BufferedWriter(fw);
//    			bw.write(content);
//    			bw.close();
//     
//    			System.out.println("Done");
//     
//    		} catch (IOException e) {
//    			e.printStackTrace();
//    		}
//        }
//        
        
        // test join vs indexed join
        
        start=System.currentTimeMillis();    
        Table jST = student.join("id", "studId", transcript);
        timeTaken=System.currentTimeMillis() - start;
        out.println("Nested loop join " + timeTaken + "ms");
        // writing here
        System.out.println("Writing the result it to a file");
        try {
			 
			String name1 = "/output/OutputJoin_NLJ"+Table.indexType+".txt";
        	String content = "Time for join (NLJ): "+timeTaken+" ms";
			File file = new File(Table.DIR+name1);
 
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
 
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content);
			bw.close();
 
			System.out.println("Done");
 
		} catch (IOException e) {
			e.printStackTrace();
		}
    
        start=System.currentTimeMillis();
        jST = student.indexedJoin("id", "studId", transcript);
        timeTaken=System.currentTimeMillis() - start;
        out.println("indexed join " + timeTaken + "ms");
     // writing here
        System.out.println("Writing the result it to a file");
        try {
			 
			String name2 = "/output/OutputJoin_IndexedJoin"+Table.indexType+".txt";
        	String content = "Time for join (indexed): "+timeTaken+" ms";
			File file = new File(Table.DIR+name2);
 
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
 
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content);
			bw.close();
 
			System.out.println("Done");
 
		} catch (IOException e) {
			e.printStackTrace();
		}

        // print out student table Linear Hash Index
        if(Table.indexType == "LinHashMap_2") {
        	((LinHashMap_2<KeyType,Comparable[]>)student.index).print();
        	
        	String name = "outputLinHashMap_2(Guannan).txt";
    		System.out.println("writing the output in here:"+">"+Table.DIR+name);
    		try {
    			 
    			String content = ((LinHashMap_2<KeyType,Comparable[]>)student.index).toString();
     
    			
    			File file = new File(Table.DIR+name);
     
    			// if file doesnt exists, then create it
    			if (!file.exists()) {
    				file.createNewFile();
    			}
     
    			FileWriter fw = new FileWriter(file.getAbsoluteFile());
    			BufferedWriter bw = new BufferedWriter(fw);
    			bw.write(content);
    			bw.close();
     
    			System.out.println("Done");
     
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
            
        }
        else if(Table.indexType == "BPTree_Arya") {
        	StringWriter oos = new StringWriter();
    		((BPTree_Arya<KeyType,Comparable[]>)student.index).printXml(oos);
    		
    		String name = "outputBPTree_Arya.txt";
    		System.out.println("writing the output in here:"+">"+Table.DIR+name);
    		try {
    			 
    			String content = oos.toString();
     
    			
    			File file = new File(Table.DIR+name);
     
    			// if file doesnt exists, then create it
    			if (!file.exists()) {
    				file.createNewFile();
    			}
     
    			FileWriter fw = new FileWriter(file.getAbsoluteFile());
    			BufferedWriter bw = new BufferedWriter(fw);
    			bw.write(content);
    			bw.close();
     
    			System.out.println("Done");
     
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
    		
        }
        
        else if(Table.indexType == "ExtHashMapTestCases") {
        	((ExtHashMapTestCases<KeyType,Comparable[]>)student.index).print();
        	
        }
        
		
       
    
        
	} // main

} // StudentDB class

