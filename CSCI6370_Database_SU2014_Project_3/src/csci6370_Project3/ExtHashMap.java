package csci6370_Project3;



/************************************************************************************
 * @file ExtHashMap.java
 *
 * @author  John Miller
 */

import java.io.*;
import java.lang.reflect.Array;

import static java.lang.System.out;

import java.util.*;



/************************************************************************************
 * This class provides hash maps that use the Extendable Hashing algorithm.  Buckets
 * are allocated and stored in a hash table and are referenced using directory dir.
 */
public class ExtHashMap <K, V>
       extends AbstractMap <K, V>
       implements Serializable, Cloneable, Map <K, V>
{
    /** The number of slots (for key-value pairs) per bucket.
     */
    private static final int SLOTS = 4;

    /** The class for type K.
     */
    private final Class <K> classK;

    /** The class for type V.
     */
    private final Class <V> classV;

    /********************************************************************************
     * This inner class defines buckets that are stored in the hash table.
     */
    private class Bucket 
    {
        int  nKeys;
        K [] key;
        V [] value;
        boolean manyToOne=false;
        @SuppressWarnings("unchecked")
        Bucket ()
        {
            nKeys = 0;
            manyToOne=false;
            key   = (K []) Array.newInstance (classK, SLOTS);
            value = (V []) Array.newInstance (classV, SLOTS);
        } // constructor
    } // Bucket inner class

    /** The hash table storing the buckets (buckets in physical order)
     */
    private final List <Bucket> hTable;

    /** The directory providing access paths to the buckets (buckets in logical oder)
     */
    private final List <Bucket> dir;

    /** The modulus for hashing (= 2^D) where D is the global depth
     */
    private int mod;
    
    /** The previous modulus for hashing (= 2^D) where D is the global depth
     */
    
    private int prevMod;
    /** The number of buckets
     */
    private int nBuckets;

    /** Counter for the number buckets accessed (for performance testing).
     */
    private int count = 0;
    
    /** HashMap for storing mapping
     */
    
    
    private HashMap<Integer, Integer> mapping;
    

    /********************************************************************************
     * Construct a hash table that uses Extendible Hashing.
     * @param classK    the class for keys (K)
     * @param classV    the class for keys (V)
     * @param initSize  the initial number of buckets (a power of 2, e.g., 4)
     */
    public ExtHashMap (Class <K> _classK, Class <V> _classV, int initSize)
    {
        classK = _classK;
        classV = _classV;
        hTable = new ArrayList <> ();   // for bucket storage
        dir    = new ArrayList <> ();   // for bucket access
        mod    = nBuckets = initSize;
        prevMod=0;
        mapping=new HashMap<Integer, Integer>();
        for(int i=0;i<nBuckets;i++)
        {
        	int hashValue=h(i);
        	mapping.put(hashValue, hashValue);
        	dir.add(hashValue, new Bucket());
        }
    } // constructor

    /********************************************************************************
     * Return a set containing all the entries as pairs of keys and values.
     * @return  the set view of the map
     */
    public Set <Map.Entry <K, V>> entrySet ()
    {
        Set <Map.Entry <K, V>> enSet = new HashSet <> ();

        //  T O   B E   I M P L E M E N T E D
        
        
        for(Bucket bucket:dir)
        {
        	if(bucket!=null)
        	for(int ind=0;ind<SLOTS;ind++)
        	{
        		if(bucket.key[ind]!=null)
        			enSet.add(new AbstractMap.SimpleEntry<K, V>(bucket.key[ind], bucket.value[ind]));
        	}
        }
            
        return enSet;
    } // entrySet

    /********************************************************************************
     * Given the key, look up the value in the hash table.
     * @param key  the key used for look up
     * @return  the value associated with the key
     */
    public V get (Object key)
    {
        int    i = h (key);
        int mappedBucket=mapping.get(i);
        Bucket b = dir.get (mappedBucket);
    //  T O   B E   I M P L E M E N T E D
        
        count++;
        if(b!=null)
        {
          for(int ind=0;ind<SLOTS;ind++)         	  
          {
        	  if(b.key[ind]!=null && key.hashCode()==b.key[ind].hashCode())
        	  {
        		  //System.out.println("get("+key+") returned: "+b.value[ind]);
        		  return b.value[ind];
        	  }  
          }
         // System.out.println("value not found");
          return null;
        
        }
        else
        {
        	//System.out.println("value not found");
            return null;
        }
    } // get

    /********************************************************************************
     * Put the key-value pair in the hash table.
     * @param key    the key to insert
     * @param value  the value to insert
     * @return  null (not the previous value)
     */
    public V put (K key, V value)
    {
    	
        int    i = h (key); 
        int mappedBucket=mapping.get(i);
        Bucket b = dir.get (mappedBucket);
       
        //  T O   B E   I M P L E M E N T E D
           
            
        if(b.nKeys < SLOTS)
          {
        	b.key[b.nKeys] = key;
        	b.value[b.nKeys] = value;
        	b.nKeys ++;
          }
        else
          {
        	
        	split(key);
        	put(key, value);
        	}
        
        return value;
           } // put

    /********************************************************************************
     * Splits the bucket which the key points
     * @param key    the key
     * 
     */
    
    private void split(K key) {
		// TODO Auto-generated method stub
    	int    i = h (key); 
    	int mappedBucket=mapping.get(i);
        Bucket b = dir.get (mappedBucket);
        List<Integer> probKeyArray   = new ArrayList();
        
        if(!b.manyToOne) //buckets with one-one mapping which need hash function change
        {
        	int temp=i+mod;
        	prevMod=mod;
        	//changing mod or hash function 
        	mod=mod*2;
        	nBuckets=nBuckets+1;
        	
        	for(int ind=prevMod;ind<mod;ind++)
   		     {
   			   int hashValue=h(ind);
   			   Bucket b2=new Bucket();
   			   b2.manyToOne=true;
   			   dir.add(hashValue, new Bucket());
   		     }
        	
        	for(int ind=0;ind<prevMod; ind++)
        	  {
        		int hashValue=h(ind);
        		int mappedBuck=mapping.get(hashValue);
        		Bucket b2=dir.get(mappedBuck);
        		b2.manyToOne=true;
        	  }
        	
        	for(int ind=0;ind<prevMod;ind++)
   		      {
   			   int y=ind+prevMod;
   			   if(y==temp)
   		          {
   				     int hashValue=h(y);
   				     mapping.put(hashValue, hashValue);
   				     Bucket b1=dir.get(hashValue);
   				     b1.manyToOne=false;
   				     b.manyToOne=false;
   			      }
   			   else
   			      {
   				   	  int hashValue=h(y);
   				   	  mapping.put(hashValue, ind);
   			      }
   			
   		       }
            }
        
        
        else //for buckets with many-one mapping, first changing the mapping to one-one
        {
        	
        	int counting=0;
        	//getting all entries mapped to that bucket
        	for(int ind=0; ind<mapping.size();ind++)
        	{
        		if(mapping.get(ind)==mappedBucket)
        			probKeyArray.add(ind);
        	}
        	
        	//changing mapping to one-one
        	for(int ind=0;ind<probKeyArray.size();ind++)
        	{
        		int mapVal=probKeyArray.get(ind);
        		mapping.put(mapVal, mapVal);
        		Bucket b1=dir.get(mapVal);
            	b1.manyToOne=false;
        	}
        	
        }
        
        
        K[] keyArray   = (K []) Array.newInstance (classK, SLOTS);
		V[] valueArray = (V []) Array.newInstance (classV, SLOTS);
        
		for(int ind= 0; ind < SLOTS; ind++) { 
			keyArray[ind] = b.key[ind]; 
			valueArray[ind] = b.value[ind]; 
		}
		
		b.nKeys=0;

		for(int ind= 0; ind < SLOTS; ind++) { 
			b.key[ind] = null; 
			b.value[ind] =null; 
		}
		
		for(int ind=0; ind<SLOTS;ind++)
		{
			int i1= h (keyArray[ind]);
			Bucket b2 = dir.get (i1);
			b2.key[b2.nKeys] = keyArray[ind]; 
			b2.value[b2.nKeys] = valueArray[ind]; 
			
			b2.nKeys++;
			//put(b.key[ind],b.value[ind]);
		}
        
		

        
        
        
        
	}//split

	/********************************************************************************
     * Return the size (SLOTS * number of buckets) of the hash table. 
     * @return  the size of the hash table
     */
    public int size ()
    {
        return SLOTS * nBuckets;
    } // size

    /********************************************************************************
     * Print the hash table.
     */
    private void print ()
    {
        out.println ("Hash Table (Extendable Hashing)");
        out.println ("-------------------------------------------");
        out.println("\t"+"bucketIndex"+"\t"+"\t"+"SLOT NO."+"\t"+"\t"+"Key"+"\t"+"\t"+"Value");
        
        for(int i=0; i<dir.size();i++)
        {
        	
        	Bucket b=dir.get(i);
        	
        	for(int j=0;j<SLOTS;j++)
        	{
        		if(b!=null && b.key[j]!=null)
        		{
        		  out.print("\t"+(i+1)+"\t"+"\t"+"\t");
        		  out.print((j+1)+"\t"+"\t"+"\t");
        		  out.print(b.key[j]+"\t"+"\t");
        		  out.println(b.value[j]+"\t"+"\t");
        		}
        	}
        }

        //  T O   B E   I M P L E M E N T E D

        out.println ("-------------------------------------------");
    } // print

    /********************************************************************************
     * Hash the key using the hash function.
     * @param key  the key to hash
     * @return  the location of the directory entry referencing the bucket
     */
    private int h (Object key)
    {
        return key.hashCode () % mod;
    } // h

    /********************************************************************************
     * The main method used for testing.
     * @param  the command-line arguments (args [0] gives number of keys to insert)
     */
    public static void main (String [] args)
    {
        ExtHashMap <Integer, Integer> ht = new ExtHashMap <> (Integer.class, Integer.class, 11);
        int nKeys = 400;
        if (args.length == 1) nKeys = Integer.valueOf (args [0]);
        for (int i = 1; i < nKeys; i += 1) ht.put (i, i * i);
        ht.print ();
        for (int i = 0; i < nKeys; i++) {
            out.println ("key = " + i + " value = " + ht.get (i));
        } // for
        out.println ("-------------------------------------------");
        out.println ("Average number of buckets accessed = " + ht.count / (double) nKeys);
        
        System.out.println(ht.get(1));
    } // main

} // ExtHashMap class
