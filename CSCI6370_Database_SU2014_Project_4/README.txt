################################################################################################PROJECT: DATABASE PROJECT 4################################################################################################TEAM MEMBERS################################################################################################ARYABRATA BASU (MANAGER)ALEKHYA CHENNUPATIALEXIS LEIMAGUANNAN WANGBHARGABI CHAKRABARTI################################################################################################STEPS to execution:____________________1) Unzip CSCI6370_Database_SU2014_Project_4.zip.2) Look up in the Deliverables folder -> for summarized query execution analysis by the following
   team members:
	2.1. Alekhya Chennupati - MySQL.
	2.2. Bhargabi Chakrabarti - MySQL.
	2.3. Guannan Wang - PostgreSQL.

3) Look up in the Team Analysis folder for more detailed analysis reports:
	3.1. Alekhya's analysis report folder contains Alekhya’s detailed timing analysis.
	3.2. Bhargabi’s analysis report folder contains Bhargabi’s detailed timing analysis.
	3.3. Guannan’s analysis report folder contains Guannan’s detailed timing analysis.

4) For the extended tuple generator source, look up in the folder:
	/src/csci6370_Project4/Tuples.java
   	
	This is written by Alexis Leima.
TECHNICAL NOTE: We did generate 500000 tuples for both PostgreSQL and MySQL.
		Alekhya and Bhargabi tested on 100000 tuples due to machine resource limitation.
		Guannan was able to test it on 500000 tuples.
NOTE: This project being more on the side of query planning than on programming, we are not including any test cases and or class diagram. No ant builds are generated. No javadocs for this submission.
 
NOTE: All reports are in /Individual Project Reports folder.

################################################################################################

For this project, we have generated 500000 tuples using Dr. Miller’s tuple generator. This includes both the data sets for MySQL and PostgreSQL. Finally, the 6 queries were tested using the following scenario:• Un-optimized Query – timed and analyzed.• Fine tuning and adding indices over the query – timed and analyzed.

Conclusion:
Fine-tuning queries and adding indices improve the timing many folds.################################################################################################