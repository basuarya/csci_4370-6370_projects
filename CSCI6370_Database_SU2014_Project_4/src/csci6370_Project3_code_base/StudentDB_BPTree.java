package csci6370_Project3_code_base;
import static java.lang.System.out;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.Random;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import csci6370_Project4.TupleGenerator;
import csci6370_Project4.TupleGeneratorImpl;


/*****************************************************************************************
 * The StudentDB class makes a Student Database. 
 * @author Bhargabi Chakrabarti
 */
class StudentDB_BPTree implements Serializable {
	/*************************************************************************************
	 * Main method for creating, populating and querying a Student Database.
	 * 
	 * @param args
	 *            the command-line arguments
	 */
	public static void main(String[] args) {
		double start = 0.0, timeTaken;
		Random rand = new Random ();
		
		//out.println();

		TupleGenerator tupleGen=new TupleGeneratorImpl();
		
		//creating tables
		RangeSelectTable2 student = new RangeSelectTable2 ("Student", "id name address status",
				"Integer String String String", "id");

		RangeSelectTable2 professor = new RangeSelectTable2 ("Professor", "id name deptId",
					"Integer String String", "id");

		RangeSelectTable2 course = new RangeSelectTable2 ("Course", "crsCode deptId crsName descr",
				"String String String String", "crsCode");

		RangeSelectTable2 teaching = new RangeSelectTable2 ("Teaching", "crsCode semester profId",
					"String String Integer", "crsCode semester");

		RangeSelectTable2 transcript = new RangeSelectTable2 ("Transcript", "studId crsCode semester grade",
					"Integer String String String", "studId crsCode semester");

		//adding schemas to TupleGenerator
		tupleGen.addRelSchema ("Student",
                "id name address status",
                "Integer String String String",
                "id",
                null);

		tupleGen.addRelSchema ("Professor",
                "id name deptId",
                "Integer String String",
                "id",
                null);

		tupleGen.addRelSchema ("Course",
                "crsCode deptId crsName descr",
                "String String String String",
                "crsCode",
                null);

		tupleGen.addRelSchema ("Teaching",
                "crsCode semester profId",
                "String String Integer",
                "crcCode semester",
                new String [][] {{ "profId", "Professor", "id" },
                                 { "crsCode", "Course", "crsCode" }});

		tupleGen.addRelSchema ("Transcript",
                "studId crsCode semester grade",
                "Integer String String String",
                "studId crsCode semester",
                new String [][] {{ "studId", "Student", "id"},
                                 { "crsCode", "Course", "crsCode" },
                                 { "crsCode semester", "Teaching", "crsCode semester" }});
		
		
		
		// here for the sake of Arya's Linear hashing, I have made the SLOTS variable static to be re-used here.
		// Guannan's Note: I wonder why we hardcoded the number of student tuples?
		//                 I commented the LinHashMap.SLOTS line out, check it please.
		//int tups [] = new int [] {LinHashMap.SLOTS, 6, 4, 4, 6 };
		
		int student_row_num = 1000;   // we should test a different number - GW
		int transcript_row_num = 1000; // we should test a different number - GW
		int tups [] = new int [] {student_row_num, 6, 4, 4, transcript_row_num };		
	    
		
        Comparable [][][] resultTest = tupleGen.generate (tups);
        
        for (int i = 0; i < resultTest.length; i++) {
           // out.println (tables [i]);
            for (int j = 0; j < resultTest [i].length; j++) {
            	Comparable[] tup=new Comparable[resultTest[i][j].length];
                for (int k = 0; k < resultTest [i][j].length; k++) {
                	tup[k]=resultTest [i][j][k];
                	                    
                } // for
                
                // code to be appended here
                
                if(i==0)  // student table
                {
                	// conditional for index type; check indextype in Table.java - added by Arya
                	// retrying the same insert method, so no more conditions                	
                	student.insert2(tup);
                } else if(i == 4) { // transcript table
                	transcript.insert2(tup);
                }
                //out.println ();
            } // for
            //out.println ();
            
            
        } // for
        
       
     RangeSelectTable2 t_rangeSelect=student.rangeSelect();
     t_rangeSelect.print();
     
     
	} // main

} // StudentDB class

