package csci6370_Project4;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.function.*;
import java.util.stream.*;

import static java.lang.Boolean.*;
import static java.lang.System.out;

import java.io.Serializable;

import csci6370_Project3_code_base.ArrayUtil;
import csci6370_Project3_code_base.ExtHashMapTestCases;
import csci6370_Project3_code_base.KeyType;

/****************************************************************************************
 * 
 */
/**
 * This class implements relational database tables (including attribute names, domains
 * and a list of tuples.  Five basic relational algebra operators are provided: project,
 * select, union, minus join.  The insert data manipulation operator is also provided.
 * Missing are update and delete data manipulation operators.
 * 
 * @author Aryabrata Basu (Project RA Operator)
 * @author Guannan Wang (Select RA Operator)
 * @author Alekhya Chennupati (Join RA Operator)
 * @author Bhargabi Chakrabarti (Union RA Operator)
 * @author Alexis Leima (Minus RA Operator)
 */
public class Table implements Serializable {
	/**
	 * Relative path for storage directory
	 */
	public static final String DIR = "store" + File.separator;

	/**
	 * Filename extension for database files
	 */
	public static final String EXT = ".dbf";

	/**
	 * Counter for naming temporary tables.
	 */
	private static int count = 0;

	/**
	 * Table name.
	 */
	private final String name;

	/**
	 * Array of attribute names.
	 */
	private final String[] attribute;

	/**
	 * Array of attribute domains: a domain may be integer types: Long, Integer,
	 * Short, Byte real types: Double, Float string types: Character, String
	 */
	private final Class[] domain;

	/**
	 * Collection of tuples (data storage).
	 */
	private final List<Comparable[]> tuples;

	/**
	 * Primary key.
	 */
	private final String[] key;

	/**
	 * Index into tuples (maps key to tuple number).
	 */
	public Map<KeyType, Comparable[]> index;
	public static String indexType;
	//public final ExtHashMapTestCases <KeyType, Comparable[]> index;

	// By Alekhya used in join
	// This boolean variable is set to true if the tuples are equal
	private boolean areTuplesEqual = false;
	
	// ----------------------------------------------------------------------------------
	// Constructors
	// ----------------------------------------------------------------------------------

	/************************************************************************************
	 * Construct an empty table from the meta-data specifications.
	 *
	 * @param _name
	 *            the name of the relation
	 * @param _attribute
	 *            the string containing attributes names
	 * @param _domain
	 *            the string containing attribute domains (data types)
	 * @param _key
	 *            the primary key
	 */
	public Table(String _name, String[] _attribute, Class[] _domain,
			String[] _key) {
		name = _name;
		attribute = _attribute;
		domain = _domain;
		key = _key;
		tuples = new ArrayList<>();
		//index = new TreeMap<>(); // also try BPTreeMap, LinHashMap or ExtHashMap
		index = new ExtHashMapTestCases<>(KeyType.class, Comparable[].class, 4);
		//index = new LinHashMap_2<>(KeyType.class, Comparable[].class, 4);
		//indexType = "LinHashMap";
		
		//index =new LinHashMap_2<>(KeyType.class, Comparable[].class, 4);
		//index = new BPTree_Arya<>();
		//index = new BpTreeMap<>(KeyType.class, Comparable[].class);
		
		//index = new LinHashMap_2<>(KeyType.class, Comparable[].class, 4);
		// change this variable every time you change the index structure!
		//indexType = "LinHashMap_2";
		//indexType = "BPTree_Arya";
		indexType = "ExtHashMapTestCases";
		
		
	} // constructor

	/************************************************************************************
	 * Construct a table from the meta-data specifications and data in _tuples
	 * list.
	 *
	 * @param _name
	 *            the name of the relation
	 * @param _attribute
	 *            the string containing attributes names
	 * @param _domain
	 *            the string containing attribute domains (data types)
	 * @param _key
	 *            the primary key
	 * @param _tuple
	 *            the list of tuples containing the data
	 */
	public Table(String _name, String[] _attribute, Class[] _domain,
			String[] _key, List<Comparable[]> _tuples) {
		name = _name;
		attribute = _attribute;
		domain = _domain;
		key = _key;
		tuples = _tuples;
		//index = new TreeMap<>(); // also try BPTreeMap, LinHashMap or ExtHashMap
		//index = new LinHashMap_2<>(KeyType.class, Comparable[].class,4);
		index=new ExtHashMapTestCases <> (KeyType.class, Comparable[].class, 4);
		//index = new BpTreeMap<>(KeyType.class, Comparable[].class);
		//index =new LinHashMap_2<>(KeyType.class, Comparable[].class, 4);
		//index = new BPTree_Arya<>();
	} // constructor

	/************************************************************************************
	 * Construct an empty table from the raw string specifications.
	 *
	 * @param name
	 *            the name of the relation
	 * @param attributes
	 *            the string containing attributes names
	 * @param domains
	 *            the string containing attribute domains (data types)
	 */
	public Table(String name, String attributes, String domains, String _key) {
		this(name, attributes.split(" "), findClass(domains.split(" ")), _key
				.split(" "));

		out.println("DDL> create table " + name + " (" + attributes + ")");
	} // constructor

	// ----------------------------------------------------------------------------------
	// Public Methods
	// ----------------------------------------------------------------------------------

	/************************************************************************************
	 * Project the tuples onto a lower dimension by keeping only the given
	 * attributes. Check whether the original key is included in the projection.
	 *
	 * #usage movie.project ("title year studioNo")
	 *
	 * @param attributes
	 *            the attributes to project onto
	 * @return a table of projected tuples
	 * @author Aryabrata Basu
	 * @version 1.0
	 */
	public Table project(String attributes) {
		out.println("RA> " + name + ".project (" + attributes + ")");
		String[] attrs = attributes.split(" ");
		Class[] colDomain = extractDom(match(attrs), domain);
		String[] newKey = (Arrays.asList(attrs).containsAll(Arrays.asList(key))) ? key
				: attrs;

		List<Comparable[]> rows = new ArrayList<Comparable[]>();

		// T O B E I M P L E M E N T E D
		// First attempt by Arya
		// this version works!!

		Table intermediateResult = new Table(name + count++, attrs, colDomain,
				newKey, rows);

		try {

			// lambda expr.
			tuples.forEach(tup -> intermediateResult.tuples.add(extract(tup,
					attrs)));

			// old school way
			/*
			 * for (Comparable [] tup : tuples) { intermediateResult.tuples.add
			 * (extract (tup, attrs)); }
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}

		return intermediateResult;
		// return new Table (name + count++, attrs, colDomain, newKey, rows);
	} // project
	
	/************************************************************************************
	 * Set the index type
	 * 
	 * @param i
	 * 			the index type
	 */
	public void setIndex(Map<KeyType, Comparable[]> i){
		index = i;
	}

	/************************************************************************************
	 * Select the tuples satisfying the given predicate (Boolean function).
	 * 
	 * #usage movie.select (t -> t[movie.col("year")].equals (1977))
	 *
	 * @param predicate
	 *            the check condition for tuples
	 * @return a table with tuples satisfying the predicate
	 * @author Guannan Wang
	 */
//	public Table select() {
//		
//
//		List<Comparable[]> rows = null;
//
//		// T O B E I M P L E M E N T E D
//		// IMPLEMENTED
//
//		rows = new ArrayList<>();
//		Table selectResult = new Table(name + count++, attribute, domain, key,
//				rows);
//		
//		for (Map.Entry<KeyType, Comparable[]> e : index.entrySet()) {
//			
//			String k= e.getKey().toString();
//			k=k.substring(5,(k.length()-1));
//			k=k.trim();
//			int ky;
//			ky=Integer.parseInt(k);
//			Predicate<Comparable[]> predi=t -> t[this.col("id")].equals (ky);
//			
//			
//			for (Comparable[] tup : tuples.stream().filter(predi)
//					.collect(Collectors.toList())) {
//				rows.add(tup);
//			}
//			
//			
//		}
//
//		return selectResult;
//	} // select
	//there is no parameter. wrong javadoc
	
	/********************************************************/
	// this is the normal select with composite key handling. 

	    public Table select() {
	        
	        List<Comparable[]> rows = null;

	        // T O B E I M P L E M E N T E D
	        // IMPLEMENTED

	        rows = new ArrayList<>();
	        Table selectResult = new Table(name + count++, attribute, domain, key,
	                rows);
	        
	        for (Map.Entry<KeyType, Comparable[]> e : index.entrySet()) {
	            
	              KeyType k= e.getKey(); // comparable[] with key value
	              String[] keyNames = this.key; // key name array
	              //out.println(" K: " + k.key[0] + " => " + Arrays.toString(keyNames));
	      
	              Comparable[] kv = k.getKeys();
	              
	              Predicate<Comparable[]> predi = null;
	             
	              if(kv.length == 1) {
	                predi = t -> t[this.col(keyNames[0])].equals (kv[0]);
	              } else if(kv.length == 2) {
	                      predi = t -> t[this.col(keyNames[0])].equals (kv[0]) &&
	                                      t[this.col(keyNames[1])].equals (kv[1]);
	              } else if(kv.length == 3) {
	                      predi = t -> t[this.col(keyNames[0])].equals (kv[0]) &&
	                                      t[this.col(keyNames[1])].equals (kv[1]) &&
	                                      t[this.col(keyNames[2])].equals (kv[2]);
	              }
	                      
	            for (Comparable[] tup : tuples.stream().filter(predi)
	                    .collect(Collectors.toList())) {
	                rows.add(tup);
	            }
	            
	        }

	        return selectResult;
	    } // select
	/************************************************************************************
	 * Select the tuples satisfying the given key predicate (key = value). Use
	 * an index (Map) to retrieve the tuple with the given key value.
	 * 
	 * @author Guannan Wang
	 * @param keyVal 
	 *            the given key value
	 * @return a table with the tuple satisfying the key predicate
	 */
	public Table iSelect() {
		
		List<Comparable[]> rows = null;

		// T O B E I M P L E M E N T E D
		// IMPLEMENTED
		// version 1.1 
		// needs modification to tune to index structures

		rows = new ArrayList<>();
		Table selectResult = new Table(name + count++, attribute, domain, key,
				rows);
		
		
		for (Map.Entry<KeyType, Comparable[]> e : index.entrySet()) {	
			KeyType kt=e.getKey();			
			Comparable[] val=index.get(kt);
			if(val!=null)
			 rows.add(val);
		}
		
		return selectResult;
	} // select
	
	/************************************************************************************
	 * Sequential select the tuples satisfying the given key predicate (key = value). 
	 * 
	 * @author 	Alexis Leima
	 * @param 	keyVal
	 *            the given key value
	 * @return a table with tuples satisfying the key predicate
	 */
		public Table seqSelect(KeyType keyVal){
		out.println("RA> " + name + ".select (" + keyVal + ")");
		List<Comparable[]> rows = new ArrayList<>();
		
		tuples.forEach(tup -> {if(tup.equals(keyVal)) rows.add(tup);});
		
		return new Table(name + count++, attribute, domain, key, rows);
	}

	/************************************************************************************
	 * Sequential select the tuples satisfying the given predicate. 
	 * 
	 * @author 	Alexis Leima
	 * @param 	predicate
	 *            the given predicate
	 * @return a table with tuples satisfying the key predicate
	 */
	public Table seqSelect(Predicate<Comparable[]> predicate){
		out.println("RA> " + name + ".select (" + predicate + ")");
		List<Comparable[]> rows = new ArrayList<>();
		
		tuples.forEach(tup -> {if(predicate.test(tup)) rows.add(tup);});
		
		return new Table(name + count++, attribute, domain, key, rows);
	}

	/************************************************************************************
	 * Indexed select the tuples satisfying the given predicate. 
	 * 
	 * @author 	Alexis Leima
	 * @param 	predicate
	 *            the given predicate
	 * @return a table with tuples satisfying the key predicate
	 */
	public Table iSelect(KeyType keyVal){
		out.println("RA> " + name + ".select (" + keyVal + ")");
		List<Comparable[]> rows = new ArrayList<>();
		
		Comparable[] val = index.get(keyVal);
		if(val != null) rows.add(val);
		
		return new Table(name + count++, attribute, domain, key, rows);
	}
	
	// another approach to indexed select- only to be used later! NOT IN USAGE NOW!
	public Table iAryaSelect() {
		//out.println("RA> " + name + ".select (" + keyVal + ")");

		List<Comparable[]> rows = null;

		// T O B E I M P L E M E N T E D
		// IMPLEMENTED
		// version 1.1 
		// needs modification to tune to index structures

		rows = new ArrayList<>();
		Table selectResult = new Table(name + count++, attribute, domain, key,
				rows);
		
		// attempt by Arya
		for (Map.Entry<KeyType, Comparable[]> e : index.entrySet()) {	
			
			KeyType kt=e.getKey();			
			Comparable[] val=index.get(kt);
			
			if(val!=null) {
				rows.add(val);
			}
			/*else {
				System.out.println("Table does not contain " + val);
				//return null;
			}*/
		}
		
//		if(index.get(keyVal) != null) {
//			rows.add(index.get(keyVal));
//		}
//		else {
//			System.out.println("Table does not contain " + keyVal);
//			return null;
//		}
		

		return selectResult;
	} // iAryaSelect
	
	
	
	/************************************************************************************
	 * Union this table and table2. Check that the two tables are compatible.
	 * 
	 * @author Bhargabi Chakrabarti #usage movie.union (show)
	 *
	 * @param table2
	 *            the rhs table in the union operation
	 * @return a table representing the union
	 * 
	 * @version 1.1
	 */
	
	// new method starts here (java 8 compatible)
	public Table union (Table table2)
    {
        out.println ("RA> " + name + ".union (" + table2.name + ")");
        

        List <Comparable []> rows = new ArrayList<Comparable[]>();

        //  T O   B E   I M P L E M E N T E D ---- DONE
         count++;
       
        
        if (! compatible (table2)) 
            {
            System.out.println("Tables are NOT COMPATIBLE");
            return new Table(name+count,attribute, domain, key, rows);
            }
        else
        {
              //lambda expression
            this.tuples.forEach(tuple -> rows.add(tuple));
                                    
            table2.tuples.forEach(tuple ->{ if(!alreadyExist(tuple, this.tuples)){ rows.add(tuple);}});
        }
        Table tempTable=new Table(name+count,attribute, domain, key, rows);
        return tempTable ;
    } // union

	

	// / method by Bhargabi ///
	/************************************************************************************
	 * Checks if a tuple already exist in a table
	 *
	 * @param tuple
	 *            The tuple to look for in the table
	 * @param tuples2
	 *            The list of tuples that exist in the table
	 * @return true is tuple exists, false otherwise
	 */

	private boolean alreadyExist(Comparable[] tuple, List<Comparable[]> tuples2) {
		// TODO Auto-generated method stub
		for (Comparable[] tuplePresent : tuples2) {
			if (tuplePresent.equals(tuple))
				return true; // exists
		}
		return false; // not present
	}

	// / method by Bhargabi ///

	/************************************************************************************
	 * @author Alexis Leima Take the difference of this table and table2. Check
	 *         that the two tables are compatible.
	 *
	 *         #usage movie.minus (show)
	 *
	 * @param table2
	 *            The rhs table in the minus operation
	 * @return a table representing the difference
	 */
	public Table minus(Table table2) {
		out.println("RA> " + name + ".minus (" + table2.name + ")");
		if (!compatible(table2)) {
			System.out.println("Tables are NOT COMPATIBLE");
			return null;
		}

		List<Comparable[]> rows = new ArrayList<Comparable[]>();
		rows.addAll(tuples);

		Iterator<Comparable[]> it = rows.iterator();
		while (it.hasNext()) {
			if (table2.tuples.contains(it.next()))
				it.remove();
		}

		return new Table(name + count++, attribute, domain, key, rows);
	} // minus

	/************************************************************************************
	 * Join this table and table2 by performing an equijoin. Tuples from both
	 * tables are compared requiring attributes1 to equal attributes2.
	 * Disambiguate attribute names by append "2" to the end of any duplicate
	 * attribute name.
	 *
	 * #usage movie.join ("studioNo", "name", studio) #usage movieStar.join
	 * ("name == s.name", starsIn)
	 * 
	 * @author Alekhya Chennupati
	 * @param attribute1
	 *            the attributes of this table to be compared (Foreign Key)
	 * @param attribute2
	 *            the attributes of table2 to be compared (Primary Key)
	 * @param table2
	 *            the rhs table in the join operation
	 * @return a table with tuples satisfying the equality predicate
	 */
	public Table join(String attributes1, String attributes2, Table table2) {
		out.println("RA> " + name + ".join (" + attributes1 + ", "
				+ attributes2 + ", " + table2.name + ")");

		String[] t_attrs = attributes1.split(" ");
		String[] u_attrs = attributes2.split(" ");

		List<Comparable[]> rows = null;

		// Implemented By Alekhya
		rows = new ArrayList<>();

		int[] colpos1 = new int[t_attrs.length];
		int[] colpos2 = new int[u_attrs.length];

		if (t_attrs.length == u_attrs.length) // checks if the number of attributes in join condition are same
												
			for (int i = 0; i < t_attrs.length; i++) {
				colpos1[i] = col(t_attrs[i]); // Get the attributes1 positions in table1 in to colpos1[ ]
												
				colpos2[i] = table2.col(u_attrs[i]);// Get the attributes2 positions in table2 in to colpos2[ ]
												
				if (u_attrs[i].equals(t_attrs[i])) // Check if the names of attributes in the condition are same
                {
					table2.attribute[i] = u_attrs[i].toString().concat("2");// if attribute names in both tables are same then append 2 at the end of duplicate attribute.
																			
				}
			}
		else
		{
			out.println("Can not join tables as both have different number of attributes in join condition");
		    return null; // returns null if join returns empty!
		    
		}
		Table joinedTable = new Table(name + count++, ArrayUtil.concat(
				attribute, table2.attribute), ArrayUtil.concat(domain,
				table2.domain), key, rows); // create new joinedTable which
											// stores the values of final joined
											// tuples from both tables
		try {
			for (Comparable[] tup1 : tuples) {

				for (Comparable[] tup2 : table2.tuples) {
					areTuplesEqual = false;
					checkEqualTuples(colpos1, colpos2, tup1, tup2,
							u_attrs.length);
					if (areTuplesEqual) // if the tuples are equal then join the
										// tuples
					{
						joinedTable.tuples.add(ArrayUtil.concat(tup1, tup2));
					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return joinedTable;

	} // join

	// Implemented by Alekhya
	// This recursive method checkEqualTuples checks if the tuples in the
	// attribute are same, if the tuples are same then it returns true, else
	// returns false.
	public void checkEqualTuples(int[] colpos1, int[] colpos2,
			Comparable[] tup1, Comparable[] tup2, int size) {

		if (size > 0) {
			if (tup1[colpos1[size - 1]] == tup2[colpos2[size - 1]]) {
				size -= 1;
				areTuplesEqual = true;
				checkEqualTuples(colpos1, colpos2, tup1, tup2, size);
			} else {
				areTuplesEqual = false;
			}
		}
	}
	
	
    /** 
     * only works for single attribute column join at the moment
     * further developments needed for composite key join
     * 
     * @author Guannan Wang
     * @param attributes1 - table1 primary key
     * @param attributes2 - table2 foreign key
     * @param table2 - table2
     * @return joined table
     * @status: working
     */
//    public Table indexedJoin(String attributes1, String attributes2, Table table2) {
//      
//        List rows = new ArrayList<>();
//    
//       // ((LinHashMap_2<KeyType,Comparable[]>) index).print();
//    
//        Table iJoinResult = new Table(name + "join" + table2.name + count++, ArrayUtil.concat(attribute, table2.attribute), ArrayUtil.concat(domain,
//            table2.domain), key, rows); // create new joinedTable which
//    
//        for (Comparable[] tup : table2.tuples) {
//            String table2_key = tup[table2.col(attributes2)].toString();
//            Comparable[] kc = {table2_key};
//            KeyType k =  new KeyType(Integer.parseInt(table2_key));
//    
//            Comparable[] tup1 = index.get(k); // table 1 tuple;
//            if(tup1 != null) {
//              iJoinResult.tuples.add(ArrayUtil.concat(tup1, tup));
//            }   
//    
//        }   
//    
//        //iJoinResult.print();
//        return iJoinResult;
//    }   

	
	/* @author Alekhya Chennupati
     * @param attributes1 - table1 primary key
     * @param attributes2 - table2 foreign key
     * @param table2 - table2
     * @return joined table
     *
	*/
	
	/* @author Alekhya Chennupati
     * @param attributes1 - table1 primary key
     * @param attributes2 - table2 foreign key
     * @param table2 - table2
     * @return joined table
     *
	*/
	
	public Table  indexedJoin(String attributes1, String attributes2, Table table2) {
		
		out.println("RA> " + name + ".join (" + attributes1 + ", "
				+ attributes2 + ", " + table2.name + ")");

		List<Comparable[]> rows = null;
		rows = new ArrayList<>();
	
		int colpos2 = table2.col(attributes2);
		
		Table joinedTable = new Table(name + count++, ArrayUtil.concat(
				attribute, table2.attribute), ArrayUtil.concat(domain,
				table2.domain), key, rows); // create new joinedTable which
											// stores the values of final joined
											// tuples from both tables
		try {
			Comparable[] b;
			String secondAttrs;
			KeyType k ;
					for(Comparable[] tup2:table2.tuples)
					{	
						KeyType secondAtrs=new KeyType(Integer.parseInt(String.valueOf(tup2[colpos2])));
						b=index.get(secondAtrs);    
	
			if(b!=null)
				joinedTable.tuples.add(ArrayUtil.concat(b, tup2));	
			
		} 
		}
			catch (Exception e) {
			e.printStackTrace();
		}
		return joinedTable;

	} // join

	
	/************************************************************************************
	 * Return the column position for the given attribute name.
	 *
	 * @param attr
	 *            the given attribute name
	 * @return a column position
	 */
	public int col(String attr) {
		for (int i = 0; i < attribute.length; i++) {
			if (attr.equals(attribute[i]))
				return i;
		} // for

		return -1; // not found
	} // col

	/************************************************************************************
	 * Insert a tuple to the table.
	 *
	 * #usage movie.insert ("'Star_Wars'", 1977, 124, "T", "Fox", 12345)
	 *
	 * @param tup
	 *            the array of attribute values forming the tuple
	 * @return whether insertion was successful
	 */
	public boolean insert(Comparable[] tup) {
		out.println("DML> insert into " + name + " values ( "
				+ Arrays.toString(tup) + " )");

		if (typeCheck(tup)) {
			tuples.add(tup);
			Comparable[] keyVal = new Comparable[key.length];
			int[] cols = match(key);
			for (int j = 0; j < keyVal.length; j++)
				keyVal[j] = tup[cols[j]];
			index.put(new KeyType(keyVal), tup);
			return true;
		} else {
			return false;
		} // if
	} // insert
	
	// new insert method 
//	public boolean insert2 (Comparable [] tup)
//	{
//		out.println ("DML> insert* (using the new insert) into  " + name + " values ( " + Arrays.toString (tup) + " )");
//
//		if (typeCheck2 (tup, domain)) {
//			tuples.add (tup);
//			Comparable[] keyVal = new Comparable [key.length];
//			int[] cols = match (key);
//			for (int j = 0; j < keyVal.length; j++) {
//				keyVal[j] = tup[cols[j]];
//			}
//			
//			index.put (new KeyType (keyVal), tup);
//			
//			return true;
//		} 
//		else {
//			return false;
//		} 
//	} // new insert method
	
	// added by Guannan Wang
//	public boolean insert2 (Comparable [] tup) 
//	{
//	         //out.println ("DML> insert* (using the new insert) into  " + name + " values ( " + Arr     ays.toString (tup) + " )");
//	  
//	         out.print("insert into " + name );
//	          String values = " values (";
//	          for(int i = 0; i < tup.length; i++) {
//	             if(tup[i].getClass().getName().equals("java.lang.Integer")) {
//	                  values += tup[i] + ",";
//	              } else {
//	                  values += "'" + tup[i] + "',";
//	              }
//	          }
//	          values = values.substring(0, values.length() - 1);
//	          values += ");";
//	  
//	          out.print(values);
//	          out.println();
//	          
//	          return true;
//	}
	
	// Alekhya's re-directed o/p insert2 method.
	public boolean insert2 (Comparable [] tup)
	{
			String output;
		       String values = " values (";
		         for(int i = 0; i < tup.length; i++) {
		              if(tup[i].getClass().getName().equals("java.lang.Integer")) {
		                 values += tup[i] + ",";
		              } else {
		                 values += "'" + tup[i] + "',";
		              }
		        }
		          values = values.substring(0, values.length() - 1);
		          values += ");";
		          output = "insert into"+name+values;
		          StringWriter oos = new StringWriter();
		          
		    		String name = "OUTPUTSQL.txt";
		    		System.out.println("writing the output in here:"+">"+Table.DIR+name);
		    		try {
		    			
		    			File file = new File(Table.DIR+name);
		     
		    			// if file does not exists, then create it
		    			if (!file.exists()) {
		    				file.createNewFile();
		    			}
		     
		    			FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
		    			BufferedWriter bw = new BufferedWriter(fw);
		    			bw.write(output);
		    			bw.close();
		     
		     
		    		} catch (IOException e) {
		    			e.printStackTrace();
		    		}
		    		          
		 out.print(output);        
       //out.print(values);
		 out.println();
		 return true; 
		
		
		
	}
	
	
	/************************************************************************************
	 * Get the name of the table.
	 *
	 * @return the table's name
	 */
	public String getName() {
		return name;
	} // getName

	/************************************************************************************
	 * Print this table.
	 */
	public void print() {
		out.println("\n Table " + name);
		out.print("|-");
		for (int i = 0; i < attribute.length; i++)
			out.print("---------------");
		out.println("-|");
		out.print("| ");
		for (String a : attribute)
			out.printf("%15s", a);
		out.println(" |");
		out.print("|-");
		for (int i = 0; i < attribute.length; i++)
			out.print("---------------");
		out.println("-|");
		for (Comparable[] tup : tuples) {
			out.print("| ");
			for (Comparable attr : tup)
				out.printf("%15s", attr);
			out.println(" |");
		} // for
		out.print("|-");
		for (int i = 0; i < attribute.length; i++)
			out.print("---------------");
		out.println("-|");
	} // print

	/************************************************************************************
	 * Print this table's index (Map).
	 */
	public void printIndex() {
		out.println("\n Index for " + name);
		out.println("-------------------");
		for (Map.Entry<KeyType, Comparable[]> e : index.entrySet()) {
			out.println(e.getKey() + " -> " + Arrays.toString(e.getValue()));
		} // for
		out.println("-------------------");
	} // printIndex

	/************************************************************************************
	 * Load the table with the given name into memory.
	 *
	 * @param name
	 *            the name of the table to load
	 */
	public static Table load(String name) {
		Table tab = null;
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(
					DIR + name + EXT));
			tab = (Table) ois.readObject();
			ois.close();
		} catch (IOException ex) {
			out.println("load: IO Exception");
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
			out.println("load: Class Not Found Exception");
			ex.printStackTrace();
		} // try
		return tab;
	} // load

	/************************************************************************************
	 * Save this table in a file.
	 */
	public void save() {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(
					new FileOutputStream(DIR + name + EXT));
			oos.writeObject(this);
			oos.close();
		} catch (IOException ex) {
			out.println("save: IO Exception");
			ex.printStackTrace();
		} // try
	} // save

	// ----------------------------------------------------------------------------------
	// Private Methods
	// ----------------------------------------------------------------------------------

	/************************************************************************************
	 * Determine whether the two tables (this and table2) are compatible, i.e.,
	 * have the same number of attributes each with the same corresponding
	 * domain.
	 *
	 * @param table2
	 *            the rhs table
	 * @return whether the two tables are compatible
	 */
	private boolean compatible(Table table2) {
		if (domain.length != table2.domain.length) {
			out.println("compatible ERROR: table have different arity");
			return false;
		} // if
		for (int j = 0; j < domain.length; j++) {
			if (domain[j] != table2.domain[j]) {
				out.println("compatible ERROR: tables disagree on domain " + j);
				return false;
			} // if
		} // for
		return true;
	} // compatible

	/************************************************************************************
	 * Match the column and attribute names to determine the domains.
	 *
	 * @param column
	 *            the array of column names
	 * @return an array of column index positions
	 */
	private int[] match(String[] column) {
		int[] colPos = new int[column.length];

		for (int j = 0; j < column.length; j++) {
			boolean matched = false;
			for (int k = 0; k < attribute.length; k++) {
				if (column[j].equals(attribute[k])) {
					matched = true;
					colPos[j] = k;
				} // for
			} // for
			if (!matched) {
				out.println("match: domain not found for " + column[j]);
			} // if
		} // for

		return colPos;
	} // match

	/************************************************************************************
	 * Extract the attributes specified by the column array from tuple t.
	 *
	 * @param t
	 *            the tuple to extract from
	 * @param column
	 *            the array of column names
	 * @return a smaller tuple extracted from tuple t
	 */
	private Comparable[] extract(Comparable[] t, String[] column) {
		Comparable[] tup = new Comparable[column.length];
		int[] colPos = match(column);
		for (int j = 0; j < column.length; j++)
			tup[j] = t[colPos[j]];
		return tup;
	} // extract

	/************************************************************************************
	 * Check the size of the tuple (number of elements in list) as well as the
	 * type of each value to ensure it is from the right domain.
	 *
	 * @param t
	 *            the tuple as a list of attribute values
	 * @return whether the tuple has the right size and values that comply with
	 *         the given domains
	 * @author Bhargabi Chakrabarti, Aryabrata Basu 
	 * @version 1.1
	 */
//	private boolean typeCheck(Comparable[] t) {
//		// T O B E I M P L E M E N T E D
//
//		return true;
//	} // typeCheck
	
	private boolean typeCheck (Comparable [] t)
    { 
        //  T O   B E   I M P L E M E N T E D --DONE
        
        //checking data types
        if(this.domain.length != t.length)
          {
            System.out.println("Arity not correct");
            return false;
          }
        
        for(int i=0;i<t.length;i++)
            if(t[i].getClass().equals(this.domain[i]))
            {   
                System.out.println("Data type mismatch");
                System.out.println("Attribute "+(i+1)+": Required "+this.domain[i]+ " found "+t[i].getClass());                
                return false;                
            }        
        
        //checking if tuple already present
        if(alreadyExist(t, this.tuples))
          {
               System.out.println("Tuple already exist");
               return false;
          }
        
        return true;
    } // typeCheck
	
	// new typeCheck - Arya
	// Initial Attempt
	// status: Working!
	
	private static boolean typeCheck2 (Comparable [] tup, Class [] dom)
	{ 
		if(tup.length != dom.length) { 
			return false; 
		} 
		int total = tup.length; 
		
		for(int i = 0;  i<total; i++) { 
			if(tup[i].getClass().equals(dom[i]) == false) { 
				return false; 
			} 
		}
		return true;
	} // typeCheck
	/************************************************************************************
	 * Find the classes in the "java.lang" package with given names.
	 *
	 * @param className
	 *            the array of class name (e.g., {"Integer", "String"})
	 * @return an array of Java classes
	 */
	private static Class[] findClass(String[] className) {
		Class[] classArray = new Class[className.length];

		for (int i = 0; i < className.length; i++) {
			try {
				classArray[i] = Class.forName("java.lang." + className[i]);
			} catch (ClassNotFoundException ex) {
				out.println("findClass: " + ex);
			} // try
		} // for

		return classArray;
	} // findClass

	/************************************************************************************
	 * Extract the corresponding domains.
	 *
	 * @param colPos
	 *            the column positions to extract.
	 * @param group
	 *            where to extract from
	 * @return the extracted domains
	 */
	private Class[] extractDom(int[] colPos, Class[] group) {
		Class[] obj = new Class[colPos.length];

		for (int j = 0; j < colPos.length; j++) {
			obj[j] = group[colPos[j]];
		} // for

		return obj;
	} // extractDom

} // Table class

