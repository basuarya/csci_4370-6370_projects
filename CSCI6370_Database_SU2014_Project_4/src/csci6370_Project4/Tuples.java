package csci6370_Project4;

import java.io.Writer;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author Alexis Leima
 *
 */
public class Tuples {
	
	public Comparable[][][] table(int stu, int prof, int crs, int tran, int teach)
	{
        TupleGenerator test = new TupleGeneratorImpl ();

        test.addRelSchema ("Student",
                           "id name address status",
                           "Integer String String String",
                           "id",
                           null);
        
        test.addRelSchema ("Professor",
                           "id name deptId",
                           "Integer String String",
                           "id",
                           null);
        
        test.addRelSchema ("Course",
                           "crsCode deptId crsName descr",
                           "String String String String",
                           "crsCode",
                           null);
        
        test.addRelSchema ("Teaching",
                           "crsCode semester profId",
                           "String String Integer",
                           "crcCode semester",
                           new String [][] {{ "profId", "Professor", "id" },
                                            { "crsCode", "Course", "crsCode" }});
        
        test.addRelSchema ("Transcript",
                           "studId crsCode semester grade",
                           "Integer String String String",
                           "studId crsCode semester",
                           new String [][] {{ "studId", "Student", "id"},
                                            { "crsCode", "Course", "crsCode" },
                                            { "crsCode semester", "Teaching", "crsCode semester" }});
        
        int tups [] = new int [] { stu, prof, crs, teach, tran };
    
        return test.generate (tups);
	}

	public void Course(int i, Comparable[][][] result){
		Writer writer = null;
	
		try {
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream("course.txt"), "utf-8"));
			
			for(int j=0; j<i; ++j){
			String CourseCode = (String)result[2][j][0];
			String DeptID = (String)result[2][j][1];	
			String CourseName = (String)result[2][j][2];
			String Description = (String)result[2][j][3];
			// mySQL friendly
//			writer.write("INSERT INTO sakila.'course'('DeptID','CourseCode','CourseName','Description') "
//					+ "VALUES ('"+ DeptID + "', '"+CourseCode+"', '"+CourseName+"', '"+Description+"');\n");
			
			// postgreSQl friendly friendly
			writer.write("INSERT INTO course VALUES ('"+ DeptID + "', '"+CourseCode+"', '"+CourseName+"', '"+Description+"');\n");
			
			}
		} catch (IOException ex) {
			System.out.println("Could not write to Course file.");
		} finally {
			try {writer.close();} catch (Exception ex) {}
		}
	}
	
	public void Professor(int i, Comparable[][][] result){
		Writer writer = null;
		
		try {
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream("professor.txt"), "utf-8"));
			
			for(int j=0; j<i; ++j){
			Integer ProfID = (Integer)result[1][j][0];	
			String Name = (String)result[1][j][1];
			String DeptID = (String)result[1][j][2];
			// mySQL friendly
//			writer.write("INSERT INTO sakila.'professor'('idProfessor','Name','DeptId') VALUES ('"
//					+ ProfID + "', '"+Name+"', '"+DeptID+"');\n");
			
			// postgreSQl friendly friendly
			writer.write("INSERT INTO professor VALUES ('"
					+ ProfID + "', '"+Name+"', '"+DeptID+"');\n");
			
			}
		} catch (IOException ex) {
			System.out.println("Could not write to Course file.");
		} finally {
			try {writer.close();} catch (Exception ex) {}
		}
	}
	
	public void Student(int i, Comparable[][][] result){
		Writer writer = null;
		
		try {
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream("student.txt"), "utf-8"));
			
			for(int j=0; j<i; ++j){
			Integer ID = (Integer)result[0][j][0];	
			String Name = (String)result[0][j][1];
			String Add = (String) result[0][j][2];
			String Status = (String)result[0][j][3];
			// mySQL friendly
//			writer.write("INSERT INTO sakila.'student'('idStudent','Name','Address','Status') VALUES ('"
//					+ ID + "', '"+Name+"', '"+Add+"' '" + Status +"');\n");
			
			// postgreSQl friendly friendly
			writer.write("INSERT INTO student VALUES ('"
					+ ID + "', '"+Name+"', '"+Add+"' '" + Status +"');\n");
			
			}
		} catch (IOException ex) {
			System.out.println("Could not write to Course file.");
		} finally {
			try {writer.close();} catch (Exception ex) {}
		}
	}
	
	public void Teaching(int i, Comparable[][][] result){
		Writer writer = null;
		
		try {
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream("teaching.txt"), "utf-8"));
			
			for(int j=0; j<i; ++j){
			String crsCode = (String)result[3][j][0];	
			String sem = (String)result[3][j][1];
			Integer profID = (Integer) result[3][j][2];
			// mySQL friendly
//			writer.write("INSERT INTO sakila.'Teaching'('ProfID','CourseCode','Semester') VALUES ('"
//					+ crsCode + "', '"+sem+"', '"+profID+"');\n");
			
			
			// postgreSQl friendly friendly
			writer.write("INSERT INTO Teaching VALUES ('"
					+ crsCode + "', '"+sem+"', '"+profID+"');\n");
			
			}
		} catch (IOException ex) {
			System.out.println("Could not write to Course file.");
		} finally {
			try {writer.close();} catch (Exception ex) {}
		}
	}
	
	public void Transcript(int i, Comparable[][][] result){
		Writer writer = null;
		
		try {
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream("transcript.txt"), "utf-8"));
			
			for(int j=0; j<i; ++j){
			Integer ID = (Integer)result[4][j][0];	
			String crsCode = (String)result[4][j][1];
			String sem = (String) result[4][j][2];
			String grade = (String)result[4][j][3];
			// mySQL friendly
//			writer.write("INSERT INTO sakila.'Transcript'('StudId','CrsCode','Semester','Grade') VALUES ('"
//					+ ID + "', '"+crsCode+"', '"+sem+"' '" + grade +"');\n");
			
			// postgreSQl friendly friendly
			writer.write("INSERT INTO Transcript VALUES ('"
					+ ID + "', '"+crsCode+"', '"+sem+"' '" + grade +"');\n");
			
			}
		} catch (IOException ex) {
			System.out.println("Could not write to Course file.");
		} finally {
			try {writer.close();} catch (Exception ex) {}
		}
	}
	
	public static void main(String[] args){
		int stu = 500000;
		int prof = 500000;
		int crs = 500000;
		int tran = 500000;
		int teach = 500000;
		Tuples tuples = new Tuples();
		Comparable[][][] result = tuples.table(stu, prof, crs, tran, teach);
		tuples.Course(crs,result);
		tuples.Professor(prof,result);
		tuples.Student(stu,result);
		tuples.Teaching(teach,result);
		tuples.Transcript(tran,result);
	}
}
