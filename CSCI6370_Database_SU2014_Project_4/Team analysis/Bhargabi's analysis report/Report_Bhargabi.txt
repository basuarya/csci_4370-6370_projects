MEMBER NAME: BHARGABI CHAKRABARTI
=================================

Manager evaluation: Our manager did an excellent job, he deserves a +2.
-------------------


My Responsibilities:
--------------------

Created tables in MYSQL, ran queries. Re-ran the queries after creating indexes.
Did comparison of time performance, plotted graphs.

Special Thanks:
--------------
Alexis Leima for writing the code which uses the TupleGenerator to
generate tuples and writes the insert statements in a file.

Also, thanks to Alekhya Chennupati for telling me about the MYSQL
profiling option to get the execution time of the queries (I was
writing SQL block for this initially).

