CREATE TABLE student (
  id      INTEGER PRIMARY KEY NOT NULL,
  name    VARCHAR(50)     NOT NULL,
  address VARCHAR(250)    NOT NULL,
  status  VARCHAR(50)     NOT NULl
);

CREATE TABLE professor (
  id      INTEGER PRIMARY KEY NOT NULL,
  name    VARCHAR(50)     NOT NULL,
  deptId  VARCHAR(50)     NOT NULL
);

CREATE TABLE course (
  crsCode VARCHAR(50) PRIMARY KEY NOT NULL,
  deptId  VARCHAR(50)      NOT NULL,
  crsName VARCHAR(50)      NOT NULL,
  descr   VARCHAR(50)      NOT NULL
);

CREATE TABLE teaching (
  crsCode  VARCHAR(50)  NOT NULL,
  semester VARCHAR(50)  NOT NULL,
  profId   INTEGER      NOT NULL,
  PRIMARY KEY (crsCode, semester),
  CONSTRAINT teaching_professor_fkey FOREIGN KEY (profId)
    REFERENCES professor (id),
  CONSTRAINT teaching_course_fkey FOREIGN KEY (crsCode)
    REFERENCES course (crsCode)
);

CREATE TABLE transcript (
  studId INTEGER NOT NULL,
  crsCode VARCHAR(50) NOT NULL,
  semester VARCHAR(50) NOT NULL,
  grade VARCHAR(50) NOT NULL,
  PRIMARY KEY (studId, crsCode, semester),
  CONSTRAINT transcript_student_fkey FOREIGN KEY (studId)
    REFERENCES student (id),
  CONSTRAINT transcript_course_fkey FOREIGN KEY (crsCode)
    REFERENCES course (crsCode),
  CONSTRAINT transcript_teaching_fkey FOREIGN KEY (crsCode, semester)
    REFERENCES teaching (crsCode, semester)
);
