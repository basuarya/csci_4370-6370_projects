<jsp:include page="header.jsp" />
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PhotoAssignment</title>
</head>
<body>

<h1>Cases</h1>
<table border="1">
<tr>
    <th>CaseID</th>
    <th>Description</th>
    
 </tr>
<s:iterator value="caseList">

<tr>
<td><s:property value="caseId"/></td> 
<td><s:property value="description"/></td>  
</tr> 
 
</s:iterator>

</table><br/>

<h1>Photos</h1>
<table border="1">
<tr>
<th>PhotoID</th>
<th>CaseID</th>
<th>Image</th>
 </tr>

<s:iterator value="allpictures" status="statusVar">

<tr>
<td><s:property value="photoid"/></td>
<td><s:property value="caseid"/></td>
<td><img src=<s:property/> alt="image">
</tr> 
 
</s:iterator>

</table><br/>
<h1>Assign Photos to a Case</h1>
<s:form action="photoAssignment">
   <s:radio label="Case ID" name="caseSelected" list="cases" />
   <s:checkboxlist label="Photo Ids" name="idSelected" list="picIDs" />
   <s:submit value="assign" align="right"/>	
 </s:form>
</body>
</html>