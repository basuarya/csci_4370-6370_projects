<jsp:include page="header.jsp" />
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CaseAssignment</title>
</head>
<body background="http://geolog.engr.uga.edu/crimeDBFiles/backGround-1.jpg">

<h1>Cases</h1>
<table border="1">
<tr>
    <th>CaseID</th>
    <th>Description</th>
    
 </tr>
<s:iterator value="caseList">

<tr>
<td><s:property value="caseId"/></td> 
<td><s:property value="description"/></td>  
</tr> 
 
</s:iterator>

</table><br/>

<h1>Officers</h1>
<table border="1">
<tr>
<th>OfficerID</th>
<th>Name</th>
 <th>Department</th>
 </tr>
<s:iterator value="officerList">

<tr>
<td><s:property value="policeId"/></td> 
<td><s:property value="lastName"/>,<s:property value="firstName"/></td> 
<td><s:property value="department"/></td>   
</tr> 
 
</s:iterator>

</table><br/>
<h1>Assign Officers to a Case</h1>
<s:form action="policeAssignment">
   <s:radio label="Case ID" name="idSelected" list="cases" />
   <s:checkboxlist label="Police Ids" name="policeIds" list="polices" />
   <s:submit value="assign" align="right"/>	
 </s:form>
</body>
</html>