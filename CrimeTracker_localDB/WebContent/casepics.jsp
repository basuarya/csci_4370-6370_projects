<jsp:include page="header.jsp" />
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Case Pictures</title>
</head>
<body>

<h1>Case Pictures</h1><br/>

<table border="1">
<tr>
<th>PhotoID</th>
<th>CaseID</th>
<th>Image</th>
</tr>
<s:iterator value="casePics" status="statusVar">

    <tr>
    <td><s:property value="photoid"/></td>
    <td><s:property value="caseid"/></td>
    <!--  <td><img src=<s:property/> alt="image">-->
    <!-- "http://geolog.engr.uga.edu/crimeDBFiles/testGetImages.php?id=" -->
    <td><img src="http://geolog.engr.uga.edu/crimeDBFiles/testGetImages.php?id=${photoid}" width="125" height="125" border="1"/>
    </tr> 
    
</s:iterator>

</table><br/>
</body>
</html>