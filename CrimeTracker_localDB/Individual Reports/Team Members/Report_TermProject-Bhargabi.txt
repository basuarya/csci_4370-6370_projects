Bhargabi Chakrabarti
====================

1)Setting up struts and initial structure.
2)Login, Logout action.
3)Session handling after login and removing session during logout.
4)Password authentication and MD5 hashing
5)DBConnection class to connect to DB
6)Writing business service methods for cases and the case action class
7)Designing case related JSPs and login jsp.
