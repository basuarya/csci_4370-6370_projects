Project 3 Readme (Report)

Group Manager: Aryabrata Basu
Group Members: Alekhya Chennupati, Bhargabi Chakrabarti, Guannan Wang, Kathryn Leima	
Author: Guannan Wang


1. Introduction
In this project, our group completed a crime tracker system for police officers. Using our system, the police officers are able to view all the cases, assign each case to several police officers, view all the available police officers, associate different police officers with different specialties, view all the criminals, associate criminals with different cases. Once this system has been completed entirely, it could be really helpful to the police.

As a programmer, I tried different database schema designs based on BCNF, 3NF, UML and 4NF and finally decided to go with the UML and 4NF one which is the clearest design. In addition, I complete the police actions and police services such as view all police officers, add a new police officer, edit and delete a speciafic police officer. Assign different police officer with different specialties.

2. Manager's Performance
Our group manager, Aryabrata Basu, did an excellent job in coordinating the entire group to complete this project. Therefore, he definitely deserves +2 (Exceeds Expectations) in this project.
