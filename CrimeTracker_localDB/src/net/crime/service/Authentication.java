package net.crime.service;

//imports for jdbc connection
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import net.crime.config.DBConnect;

// imports for MD5
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class Authentication {
	
	
	//Authenticates password
	public boolean authenticate(String id, String password)
	{
		Connection c=DBConnect.getConnection();
		
		Statement stmt = null;
	    String query = "select password from test.login where username= '" +id+"'";
	    String hashedPassword=null,dbPassword=null;           
	    try {
	        stmt = c.createStatement();
	        ResultSet rs = stmt.executeQuery(query);
	        while (rs.next()) {
	        	dbPassword=rs.getString("Password");
	        }
	        
	        System.out.println("Password from database: "+dbPassword);
	    } catch(Exception e ) {
	        e.printStackTrace();
	    } finally {
	    	    try {
	    		   c.close();
	               if (stmt != null) 
				      stmt.close();				      
			         } catch (SQLException e) {
				           e.printStackTrace();
			          } 
	              }
	             
	     
	 hashedPassword=md5(password);
	 
	 if(hashedPassword.equalsIgnoreCase(dbPassword))
		return true;
	 else
		 return false;
	}
	
	
	//Generates hash value for password
	private String md5(String input) {
        
        String md5 = null;
         
        if(null == input) return null;
         
        try {
             
        //Create MessageDigest object for MD5
        MessageDigest digest = MessageDigest.getInstance("MD5");
         
        //Update input string in message digest
        digest.update(input.getBytes(), 0, input.length());
 
        //Converts message digest value in base 16 (hex)
        md5 = new BigInteger(1, digest.digest()).toString(16);
 
        } catch (NoSuchAlgorithmException e) {
 
            e.printStackTrace();
        }
        return md5;
    }

}
