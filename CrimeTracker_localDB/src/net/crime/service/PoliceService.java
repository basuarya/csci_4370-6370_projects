package net.crime.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.ArrayList;

import net.crime.config.DBConnect;
import net.crime.objects.Police;
import net.crime.objects.Specialization;

public class PoliceService {
	List<Police> policeList = new ArrayList<Police>();
	
	private ArrayList<Integer> specialtyIds = new ArrayList<Integer>();
	
	private Police police;
	
	public List<Police> getPoliceList() {
		return policeList;
	}
	
	public void setPoliceList(List<Police> policeList) {
		this.policeList = policeList;
	}
	
	public List<Police> showPolice() {
         
		
		Connection c=DBConnect.getConnection();		
		Statement stmt = null;
	    String query = "select * from test.police";	   
	    
	    
	    try {
	        stmt = c.createStatement();
	        ResultSet rs = stmt.executeQuery(query);
	        while (rs.next()) {
	        	Police p = new Police();
	        	p.setPoliceId(rs.getInt(1));
	        	p.setFirstName(rs.getString(2));
	        	p.setLastName(rs.getString(3));
	        	p.setDepartment(rs.getString(4));
	        	p.setContactNumber(rs.getString(5));
	        	p.setGender(rs.getString(6));
	        	
	        	
	        	Statement stmt2 = null;
	        	//String sql = "select sid from test.police_specialization where policeid = " + rs.getInt(1);
	        	String sql = "select ps.sid, s.s_name from test.police_specialization ps, test.specialization s where s.sid = ps.sid and policeid = " + rs.getInt(1);
	        	
	        	stmt2 = c.createStatement();
	        	ResultSet rs2 = stmt2.executeQuery(sql);
	        	ArrayList<Integer> a = new ArrayList<Integer>();
	        	ArrayList<String> b = new ArrayList<String>();
	        	
	        	 while (rs2.next()) {
	       
	        		 a.add(new Integer(rs2.getInt(1)));
	        		 
	        		 b.add(rs2.getString(2));
	        	 }
	        	
	        	p.setSpecialtyIds(a);
	         	
	        	
	        	
	        	p.setSpecialtyNames(b);
	        	
	        	
	        	policeList.add(p);
	        }
	        return policeList;
	        
	    } catch(Exception e ) {
	        e.printStackTrace();
	    } finally {
	    	try {
	    		   c.close();
	               if (stmt != null) 
				      stmt.close();				      
			         } catch (SQLException e) {
				           e.printStackTrace();
			          } 
	              }
		return policeList;
	}
	
	public Police listPoliceById( int policeId) {
		
		Police police = new Police();
		Connection c=DBConnect.getConnection();		
		Statement stmt = null;
	    String query = "select * from test.police where policeid = " + policeId;	             
	    try {
	        stmt = c.createStatement();
	        ResultSet rs = stmt.executeQuery(query);
	        while (rs.next()) {
	        	
	        	police.setPoliceId(rs.getInt(1));
	        	police.setFirstName(rs.getString(2));
	        	police.setLastName(rs.getString(3));
	        	police.setDepartment(rs.getString(4));
	        	police.setContactNumber(rs.getString(5));
	        	police.setGender(rs.getString(6));
	        	
	        	Statement stmt2 = null;
	        	//String sql = "select ps.sid, s.s_name from test.police_specialization ps, test.specialization s where s.sid = ps.sid and policeid = " + rs.getInt(1);
	        	String sql = "select sid from test.police_specialization where policeid = " + rs.getInt(1);
	        	stmt2 = c.createStatement();
	        	ResultSet rs2 = stmt2.executeQuery(sql);
	        	
	        	ArrayList<Integer> a = new ArrayList<Integer>();
	        	
	        	 while (rs2.next()) {
	        		
	        		 a.add(new Integer(rs2.getInt(1)));
	        		 
	        		
	        	 }
	        	
	        	police.setSpecialtyIds(a);
	       
	        	
	        	
	            return police;
	        }
	        
	    } catch(Exception e ) {
	        e.printStackTrace();
	    } finally {
	    	try {
	    		   c.close();
	               if (stmt != null) 
				      stmt.close();				      
			         } catch (SQLException e) {
				           e.printStackTrace();
			          } 
	              }
		return police;
	 
	}
	
    public ArrayList<Specialization> getAllSpecialties( ) {
		
	    ArrayList<Specialization> list = new ArrayList<Specialization>();
		Connection c=DBConnect.getConnection();		
		Statement stmt = null;
	    String query = "select * from test.specialization";
	    
	    try {
	        stmt = c.createStatement();
	        ResultSet rs = stmt.executeQuery(query);
	        while (rs.next()) {
	        	Specialization s = new Specialization();
	        	s.setSid(rs.getInt(1));
	        	s.setSName(rs.getString(2));
	        	s.setSDesc(rs.getString(3));
	        	list.add(s);
	            
	        }
	        
	        return list;
	        
	    } catch(Exception e ) {
	        e.printStackTrace();
	    } finally {
	    	try {
	    		   c.close();
	               if (stmt != null) 
				      stmt.close();				      
			         } catch (SQLException e) {
				           e.printStackTrace();
			          } 
	              }
		return list;
	 
	}
	public void updatePolice(Police p) {
		
		
		Connection c=DBConnect.getConnection();		
		PreparedStatement pstmt = null;
	    String sql = "update test.police set first_name=?, last_name=?, dept=?, contact_number=?, gender=? where policeid=?";
	    
	    try {
	        pstmt = c.prepareStatement(sql);
	        pstmt.setString(1, p.getFirstName());
	        pstmt.setString(2, p.getLastName());
	        pstmt.setString(3, p.getDepartment());
	        pstmt.setString(4, p.getContactNumber());
	        pstmt.setString(5, p.getGender());
	        pstmt.setInt(6, p.getPoliceId());
	        pstmt.executeUpdate();
	        
	        
	        
	        sql = "delete from  test.police_specialization where policeid=?";
	        pstmt = c.prepareStatement(sql);
	        pstmt.setInt(1, p.getPoliceId());
	        pstmt.executeUpdate();
		    
	        
	        // insert police_specialization table
	        sql = "insert into test.police_specialization (policeid,sid) values("+p.getPoliceId()+",?)";
	        pstmt = c.prepareStatement(sql); 
	        for(Integer i : p.getSpecialtyIds()) {
	          pstmt.setInt(1,i.intValue());
	          pstmt.executeUpdate();
	        }
	        
	        
	        
	        
	        
	        
	        
	        
	        
	    } catch(Exception e ) {
	        e.printStackTrace();
	    } finally {
	    	try {
	    		c.close();
	            if (pstmt != null) 
				      pstmt.close();				      
			    } catch (SQLException e) {
		          e.printStackTrace();
			} 
	     }
	
	}
	
    public void addPolice(Police p) {
		
		
		Connection c=DBConnect.getConnection();		
		PreparedStatement pstmt = null;
	    String sql = "insert into test.police (first_name, last_name, dept, contact_number, gender) values (?,?,?,?,?)";
	    
	    try {
	        pstmt = c.prepareStatement(sql);
	        pstmt.setString(1, p.getFirstName());
	        pstmt.setString(2, p.getLastName());
	        pstmt.setString(3, p.getDepartment());
	        pstmt.setString(4, p.getContactNumber());
	        pstmt.setString(5, p.getGender());
	        pstmt.executeUpdate();
	        
	        // insert police_specialization table
	        for(Integer i : p.getSpecialtyIds()) {
	        
	          String sql2 = "insert into test.police_specialization (policeid,sid) values(LAST_INSERT_ID(),?)";
	          PreparedStatement pstmt2 = c.prepareStatement(sql2);
	          pstmt2.setInt(1,i.intValue());
	          pstmt2.execute();
	        }
	        
	        
	        
	        
	    } catch(Exception e ) {
	        e.printStackTrace();
	    } finally {
	    	try {
	    		c.close();
	            if (pstmt != null) 
				      pstmt.close();				      
			    } catch (SQLException e) {
		          e.printStackTrace();
			} 
	     }
	
	}
	
	public void deletePolice(int policeId) {
		
		
		Connection c=DBConnect.getConnection();		
		PreparedStatement pstmt = null;
	    String sql = "delete from test.police where policeid = ?";	             
	    try {
	        pstmt = c.prepareStatement(sql);
	        pstmt.setInt(1, policeId);
	        pstmt.executeUpdate();
	        
	    } catch(Exception e ) {
	        e.printStackTrace();
	    } finally {
	    	try {
	    		c.close();
	            if (pstmt != null) 
				      pstmt.close();				      
			    } catch (SQLException e) {
		          e.printStackTrace();
			} 
	     }
		
	}

}
