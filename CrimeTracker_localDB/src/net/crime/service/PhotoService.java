package net.crime.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Blob;
import java.util.ArrayList;

import net.crime.config.DBConnect;
import net.crime.objects.Photo;

public class PhotoService {

		public boolean delete(int idSelected) {
			Connection c=DBConnect.getConnection();
			Statement stmt = null;
			String sql = "DELETE FROM test.photo where photoid="+idSelected;
			try {
		        stmt = c.createStatement();
		        stmt.executeUpdate(sql);	        
		        
		    } catch(Exception e ) {
		        e.printStackTrace();
		    } finally {
		    	try {
		    		   c.close();
		               if (stmt != null) 
					      stmt.close();				      
				         } catch (SQLException e) {
					           e.printStackTrace();
				          } 
		              }
		
			return true;

		}
					

		
	public boolean assign(int idSelected, ArrayList<Photo> photoIds) {
		Connection c=DBConnect.getConnection();
		Statement stmt = null;
		
		try {
			stmt = c.createStatement();
			String sql=null;
			
			for(Photo i:photoIds)
			 {
					sql = "UPDATE test.photo SET crimeid="+idSelected+
							" WHERE photoid="+i.getPhotoid();
	               stmt.executeUpdate(sql);					
			 }
									
	    } catch(Exception e ) {
	        e.printStackTrace();
	    } finally {
	    	try {
	    		   c.close();
	               if (stmt != null) 
				      stmt.close();				      
			         } catch (SQLException e) {
				           e.printStackTrace();
			          } 
	              }


		return true;
	}


	public ArrayList<Photo> findByCase(int idSelected) {
		ArrayList<Photo> picList=new ArrayList<Photo>();
		Connection c=DBConnect.getConnection();		
		Statement stmt = null;
	    String query = "select * from test.photo where caseid="+idSelected;	             
	    try {
	        stmt = c.createStatement();
	        ResultSet rs = stmt.executeQuery(query);
	        while (rs.next()) {
	        	Photo cs=new Photo();
	        	cs.setCaseid(rs.getInt("caseid"));
	        	cs.setPhotoid(rs.getInt("photoid"));
	        	Blob blob = rs.getBlob("image");
	        	byte[] img = blob.getBytes(blob.length(),1);
	        	cs.setImage(img);
	        	picList.add(cs);
	        }
	        
	    } catch(Exception e ) {
	        e.printStackTrace();
	    } finally {
	    	try {
	    		   c.close();
	               if (stmt != null) 
				      stmt.close();				      
			         } catch (SQLException e) {
				           e.printStackTrace();
			          } 
	              }
		return picList;

		}
	
	public ArrayList<Photo> getAllPictures() {
		
		ArrayList<Photo> pictemp=new ArrayList<Photo>();
		Connection c=DBConnect.getConnection();		
		Statement stmt = null;
	    String query = "select * from test.photo";	             
	    try {
	        stmt = c.createStatement();
	        ResultSet rs = stmt.executeQuery(query);
	        while (rs.next()) {
	        	
	        	int id=rs.getInt("photoid");
	        	int cid = rs.getInt("caseid");
	        	Blob blob = rs.getBlob("image");
	        	byte[] img = blob.getBytes(blob.length(),1);
	        	Photo temp = new Photo();
	        	temp.setPhotoid(id);
	        	temp.setCaseid(cid);
	        	temp.setImage(img);
	        	pictemp.add(temp);	        	
	        }	        
	        
	    } catch(Exception e ) {
	        e.printStackTrace();
	    } finally {
	    	try {
	    		   c.close();
	               if (stmt != null) 
				      stmt.close();				      
			         } catch (SQLException e) {
				           e.printStackTrace();
			          } 
	              }
		return pictemp;		

	}
	
	
	public ArrayList<Integer> getAllPhotoIDs() {
		
		ArrayList<Integer> ids=new ArrayList<Integer>();
		Connection c=DBConnect.getConnection();		
		Statement stmt = null;
	    String query = "select photoid" +
	    		       " from test.photo";	             
	    try {
	        stmt = c.createStatement();
	        ResultSet rs = stmt.executeQuery(query);
	        while (rs.next()) {
	        	
	        	Integer id=rs.getInt("photoid");
	        	ids.add(id);	        	
	        }	        
	        
	    } catch(Exception e ) {
	        e.printStackTrace();
	    } finally {
	    	try {
	    		   c.close();
	               if (stmt != null) 
				      stmt.close();				      
			         } catch (SQLException e) {
				           e.printStackTrace();
			          } 
	              }
		return ids;		

	}

}
