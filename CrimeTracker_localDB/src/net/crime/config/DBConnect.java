package net.crime.config;

import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.eclipse.jdt.internal.compiler.ast.ThisReference;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DBConnect {
	
	private static String driver, url, id, pswd;
	public String getDriver() {
		return driver;
	}

	public static void setDriver(String driver) {
		DBConnect.driver = driver;
	}

	public static String getUrl() {
		return url;
	}

	public static void setUrl(String url) {
		DBConnect.url = url;
	}

	public static String getPswd() {
		return pswd;
	}

	public static void setPswd(String pswd) {
		DBConnect.pswd = pswd;
	}

	public static String getId() {
		return id;
	}

	public static void setId(String id) {
		DBConnect.id = id;
	}
	
	
	public static Connection getConnection() {
		// TODO Auto-generated method stub
		initParam();
		try {
			Class.forName(driver);
	
		Connection connection = null;
	    connection = DriverManager.getConnection(url,id, pswd);
			if (connection != null) {
				System.out.println("Connected to database!!");
				return connection;
			} else {
				System.out.println("Failed to make connection!");			
			   return null;
		    }
		} 
	  catch (ClassNotFoundException e) {
		System.out.println("Where is your MySQL JDBC Driver?");
		e.printStackTrace();
		return null;
	       }
	  catch (SQLException e) {
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return null;
		   }
	 
		}

	private static void initParam()
	{
		try {
			 
			// old way
			//File fXmlFile = new File("C:\\Users\\aryabrata\\Desktop\\DB_SU2014_Projects\\Demo\\conf.xml");
			
			URL url = DBConnect.class.getClassLoader().getResource("conf.xml");
			File fXmlFile = new File(url.getPath());
			
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		    Document doc = dBuilder.parse(fXmlFile);
		 
		    String s;
		    doc.getDocumentElement().normalize();
		 
		    NodeList listOfConfigs = doc.getElementsByTagName("Configuration");

		    //System.out.println(listOfConfigs.getLength());    
		   
		    for(int i=0; i<listOfConfigs.getLength();i++)
		    {
		        Node n=listOfConfigs.item(i);
		       
		        if(n.getNodeType() == Node.ELEMENT_NODE){


		            Element firstConfigElement = (Element)n;

		            //-------                   
		            NodeList ifaceList = firstConfigElement.getElementsByTagName("Driver");
		            Element ifaceElement = (Element)ifaceList.item(0);
		            NodeList textIfaceList = ifaceElement.getChildNodes();
		            s = ((Node)textIfaceList.item(0)).getNodeValue().trim();
		            System.out.println(s);
		            setDriver(s);
		           
		           
		             ifaceList = firstConfigElement.getElementsByTagName("Url");
		            ifaceElement = (Element)ifaceList.item(0);
		            textIfaceList = ifaceElement.getChildNodes();
		            s = ((Node)textIfaceList.item(0)).getNodeValue().trim();
		            System.out.println(s);
		            setUrl(s);
		           
		            ifaceList = firstConfigElement.getElementsByTagName("id");
		            ifaceElement = (Element)ifaceList.item(0);
		            textIfaceList = ifaceElement.getChildNodes();
		            s = ((Node)textIfaceList.item(0)).getNodeValue().trim();
		            System.out.println(s);
		            setId(s);
		            
		            ifaceList = firstConfigElement.getElementsByTagName("pswd");
		            ifaceElement = (Element)ifaceList.item(0);
		            textIfaceList = ifaceElement.getChildNodes();
		            s = ((Node)textIfaceList.item(0)).getNodeValue().trim();
		            System.out.println(s);
		            setPswd(s);
		           
		        }
		    }
		    } catch (Exception e) {
		    e.printStackTrace();
		    
		    }
		 

	}


}
