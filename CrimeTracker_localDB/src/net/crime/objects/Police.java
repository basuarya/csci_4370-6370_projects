package net.crime.objects;

import java.util.ArrayList;

public class Police {
	
	private int policeId;
	private String firstName;
	private String lastName;
	private String department;
	private String gender;
	private String contactNumber;
	private String role_type;
	
	 private ArrayList<Integer> specialtyIds = new ArrayList<Integer>();
	 private ArrayList<String> specialtyNames = new ArrayList<String>();
	 
	 public ArrayList<Integer> getSpecialtyIds() {
	    	
	    	return specialtyIds;
	    }
	    
	    public void setSpecialtyIds(ArrayList<Integer> specialtyIds) {
	    	
	    	this.specialtyIds = specialtyIds;
	    }
	    
	 public ArrayList<String> getSpecialtyNames() {
	   return specialtyNames;
	 }
	 public void setSpecialtyNames(ArrayList<String> specialtyNames) {
		 this.specialtyNames = specialtyNames;
	 }

	
	public int getPoliceId() {
		return policeId;
	}
	public void setPoliceId(int policeId) {
		this.policeId = policeId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	   public String getContactNumber() {
	       return contactNumber;
	    }   
	    public void setContactNumber(String contactNumber) {
	        this.contactNumber = contactNumber;
	    }   


	    
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getRole_type() {
		return role_type;
	}
	public void setRole_type(String role_type) {
		this.role_type = role_type;
	}
	

}
