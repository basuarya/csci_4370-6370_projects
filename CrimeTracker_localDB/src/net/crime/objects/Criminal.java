package net.crime.objects;

public class Criminal {

	private int criminalId;
	private String criminalFirstName;
	private String criminalLastName;
	private String criminalGender;
	private String criminalDescription;
	
	public int getCriminalId() {
		return criminalId;
	}
	public void setCriminalId(int criminalId) {
		this.criminalId = criminalId;
	}
	public String getCriminalFirstName() {
		return criminalFirstName;
	}
	public void setCriminalFirstName(String criminalFirstName) {
		this.criminalFirstName = criminalFirstName;
	}
	public String getCriminalLastName() {
		return criminalLastName;
	}
	public void setCriminalLastName(String criminalLastName) {
		this.criminalLastName = criminalLastName;
	}
	public String getCriminalGender() {
		return criminalGender;
	}
	public void setCriminalGender(String criminalGender) {
		this.criminalGender = criminalGender;
	}
	public String getCriminalDescription() {
		return criminalDescription;
	}
	public void setCriminalDescription(String criminalDescription) {
		this.criminalDescription = criminalDescription;
	}

}
