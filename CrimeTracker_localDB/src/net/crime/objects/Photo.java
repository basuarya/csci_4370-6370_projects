package net.crime.objects;

public class Photo {
private int photoid;
private int caseid;
private byte[] image;

public int getPhotoid() {
	return photoid;
}

public void setPhotoid(int photoid) {
	this.photoid = photoid;
}

public int getCaseid() {
	return caseid;
}

public void setCaseid(int caseid) {
	this.caseid = caseid;
}

public byte[] getImage(){
	return image;
}

public void setImage(byte[] image){
	this.image = image;
}


}
