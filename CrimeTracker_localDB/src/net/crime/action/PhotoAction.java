package net.crime.action;

import java.util.ArrayList;

import net.crime.service.PhotoService;
import net.crime.objects.Case;
import net.crime.service.CaseService;
import net.crime.objects.Photo;

//import javax.servlet.http.HttpServletResponse;
//import org.apache.struts2.ServletActionContext;
//import com.raysep.maxlist.domain.post.image.Image;

public class PhotoAction {
	
	private ArrayList<Photo> allpictures;
	private ArrayList<Photo> casePics;
	private int idSelected;
	private int caseSelected;

	private ArrayList<Case> caseList;
	private ArrayList<Integer> cases;
	private ArrayList<Integer> picIDs;
	
	
	public ArrayList<Case> getCaseList(){
		return this.caseList;
	}
	public void setCaseList(ArrayList<Case> caseList){
		this.caseList = caseList;
	}
	public ArrayList<Photo> getCasePics(){
		return this.casePics;
	}
	public void setCasePics(ArrayList<Photo> casePics){
		this.casePics = casePics;
	}
	public ArrayList<Photo> getAllPictures(){
		return this.allpictures;
	}
	public void setAllPictures(ArrayList<Photo> allpictures){
		this.allpictures = allpictures;
	}
	public int getIdSelected(){
		return idSelected;
	}
	public void setIdSelected(int idSelected){
		this.idSelected = idSelected;
	}
	public int getCaseSelected(){
		return caseSelected;
	}
	public void setCaseSelected(int caseSelected){
		this.caseSelected = caseSelected;
	}
	public ArrayList<Integer> getCases() {
		return cases;
	}
	public void setCases(ArrayList<Integer> cases) {
		this.cases = cases;
	}
	public ArrayList<Integer> getPicIDs() {
		return picIDs;
	}
	public void setPicIDs(ArrayList<Integer> picIDs) {
		this.picIDs = picIDs;
	}
	
	
	public String deletePhoto()
	{
        PhotoService photoService=new PhotoService();
		
		if(photoService.delete(getIdSelected()))
		   return "success";
		else
			return "failure";
	}
	
	public String photoAssignment(){		
		PhotoService ps = new PhotoService();
		if(ps.assign(getIdSelected(),getAllPictures()))
			return "success";
		else
			return "failure";
	}
	
	public String  findPhotoByCase(){
		PhotoService ps = new PhotoService();
		ArrayList<Photo> photoList=ps.findByCase(getCaseSelected());		
		if(photoList.size()>0)
		{
			setCasePics(photoList);			
			return "success";
		}
		else
			return "failure";
	}
	
	public String getPictures2()
	{
		PhotoService ps = new PhotoService();
		ArrayList<Photo> pic=ps.getAllPictures();
		if(pic.size()!=0)
		{
			setAllPictures(pic);
			return "success";
		}
		else
			return "failure";
	}
	
    public String getAllCases(){
	   CaseService caseService=new CaseService();
	   ArrayList<Case> cl=caseService.getCaseList();
	   if(cl.size()!=0)
	      {
		   setCaseList(cl);
		   System.out.print(getCaseList().size());		   
		   return "success";
	      }
	   else
		   return "failure";
	   
       }
    
	public String getAllCaseIds()
	{
		CaseService caseService=new CaseService();
		ArrayList<Integer>allCaseIds=caseService.getCaseIds();
		   if(allCaseIds.size()!=0)
		      {
			   setCases(allCaseIds);
			   return "success";
			  }
		   else
		   return "failure";
	}
	
	public String getAllPicIds()
	{
		PhotoService ps = new PhotoService();
		ArrayList<Integer> p = ps.getAllPhotoIDs();
		if(p.size()>0){
			setPicIDs(p);
			return "success";
		}
		else return "failure";
	}
	
    public String getCasePhotoLists()
    {
    	CaseService caseService=new CaseService();
    	PhotoService photoService= new PhotoService();
    	ArrayList<Case>tempCaseList=caseService.getCases();
    	ArrayList<Photo>tempPhotoList=photoService.getAllPictures();
    	setCaseList(tempCaseList);
    	setAllPictures(tempPhotoList);
    	
    	ArrayList<Integer> tempCid=new ArrayList<Integer>();
    	ArrayList<Integer> tempPid=new ArrayList<Integer>();
    	
    	for(Case cs:tempCaseList)
    	{
    		int cid=cs.getCaseId();
    		tempCid.add(cid);
    	}
    	for(Photo pl:tempPhotoList)
    	{
    		int pid=pl.getPhotoid();
    		tempPid.add(pid);
    	}
    	setCases(tempCid);
    	setPicIDs(tempPid);
    	
    	return "success";
    }
	
/*	
	  private static byte[] itemImage;

	  public static void execute() {

	      try {

	          Image slika = Image.fetchOne();

	          HttpServletResponse response = ServletActionContext.getResponse();
	          response.reset();
	          response.setContentType("multipart/form-data"); 

	          itemImage = slika.getImage().getBytes(1,(int) slika.getImage().length());

	          OutputStream out = response.getOutputStream();
	          out.write(itemImage);
	          out.flush();
	          out.close();

	      } catch (SQLException e) {
	          e.printStackTrace();
	      } catch (IOException e) {
	          e.printStackTrace();
	      }


	  }

	  public byte[] getItemImage() {
	      return itemImage;
	  }

	  public void setItemImage(byte[] itemImage) {
	      this.itemImage = itemImage;
	  }*/
	
}
