package net.crime.action;

import java.util.Map;

import net.crime.service.Authentication;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;




public class LoginAction extends ActionSupport {
	
	private String id;
	private String password;
	
	
	public String execute()
	{
		System.out.println("loginAction execute");
		System.out.println(getId());
		System.out.println(getPassword());
		
		if(getId()!=null && getPassword()!=null)
		{
            Authentication authentication=new Authentication();
            if(authentication.authenticate(getId(),getPassword()))
            {
            	Map<String, Object> session = ActionContext.getContext().getSession();
            	session.put("loggedIn", "true");
            	session.put("userId", getId());
            	return "success";
            }
            else
            	return "failure";
		}
		return "error";
	}
	
	
	public String logout()
	{
		System.out.println("inside logout");
		Map<String, Object> session = ActionContext.getContext().getSession();
		session.remove("loggedIn");
		session.remove("userId");
		return "success";
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
