/************************************************************************************
 * @file LinHashMap.java
 *
 * @author  John Miller
 */

import java.io.*;
import java.lang.reflect.Array;
import static java.lang.System.out;
import java.util.*;

/************************************************************************************
 * This class provides hash maps that use the Linear Hashing algorithm. A hash
 * table is created that is an array of buckets.
 */
public class LinHashMap<K, V> extends AbstractMap<K, V> implements
		Serializable, Cloneable, Map<K, V> {
	/**
	 * The number of slots (for key-value pairs) per bucket.
	 */
	private static final int SLOTS = 4;

	/**
	 * The class for type K.
	 */
	private final Class<K> classK;

	/**
	 * The class for type V.
	 */
	private final Class<V> classV;

	/********************************************************************************
	 * This inner class defines buckets that are stored in the hash table.
	 */
	private class Bucket {
		int nKeys;
		K[] key;
		V[] value;
		Bucket next;
		int status;

		@SuppressWarnings("unchecked")
		// Bucket () { }
		Bucket(Bucket n) {
			nKeys = 0;
			key = (K[]) Array.newInstance(classK, SLOTS);
			value = (V[]) Array.newInstance(classV, SLOTS);
			next = n;
			status = 0; // check bucket split status; 0 => old ; 1 => new hash
		} // constructor
	} // Bucket inner class

	/**
	 * The list of buckets making up the hash table.
	 */
	private final List<Bucket> hTable;

	/**
	 * The modulus for low resolution hashing
	 */
	private int mod1;

	/**
	 * The modulus for high resolution hashing
	 */
	private int mod2;

	/**
	 * Counter for the number buckets accessed (for performance testing).
	 */
	private int count = 0;

	/**
	 * The index of the next bucket to split.
	 */
	private int split = 0;

	/********************************************************************************
	 * Construct a hash table that uses Linear Hashing.
	 * 
	 * @param classK
	 *            the class for keys (K)
	 * @param classV
	 *            the class for keys (V)
	 * @param initSize
	 *            the initial number of home buckets (a power of 2, e.g., 4)
	 */
	public LinHashMap(Class<K> _classK, Class<V> _classV, int initSize) {
		classK = _classK;
		classV = _classV;
		hTable = new ArrayList<>();
		for (int i = 0; i < initSize; i++) {
			// LinHashMap<K,V>.Bucket emptyBucket = new Bucket();
			LinHashMap<K, V>.Bucket b = this.new Bucket(null);
			hTable.add(b);
		}
		mod1 = initSize;
		mod2 = 2 * mod1;
	} // constructor

	/********************************************************************************
	 * Return a set containing all the entries as pairs of keys and values.
	 * 
	 * @author Guannan Wang
	 * @return the set view of the map
	 */
	public Set<Map.Entry<K, V>> entrySet() {
		Set<Map.Entry<K, V>> enSet = new HashSet<>();

		// T O B E I M P L E M E N T E D
		for(int i = 0; i < hTable.size(); i++) {
			
			Bucket b = hTable.get(i);
            if(b != null ) {
			  for(int j = 0; j < b.key.length; j++) {
				  if(b.key[j] != null) {
				
			         Map.Entry<K, V> kv = new AbstractMap.SimpleEntry<K,V>(b.key[j],b.value[j]);
			         enSet.add(kv);
				  }
			  }
			  // check overflow page
			  if(b.next != null) {
				  for(int j = 0; j < b.next.key.length; j++) {
					  if(b.next.key[j] != null) {
					
				         Map.Entry<K, V> kv = new AbstractMap.SimpleEntry<K,V>(b.next.key[j],b.next.value[j]);
				         enSet.add(kv);
					  }
				  }
				
			  }
            }
		}

		return enSet;
	} // entrySet

	/********************************************************************************
	 * Given the key, look up the value in the hash table.
	 * 
	 * @author Guannan Wang
	 * @param key
	 *            the key used for look up
	 * @return the value associated with the key
	 */
	public V get(Object key) {
		int i = h(key);

		// T O B E I M P L E M E N T E D
 		Bucket b = hTable.get(i);
		if(b.status == 1) {
			i = h2(key);
			b = hTable.get(i);
		}
		for (int j = 0; j < b.key.length; j++) {
			if (b.key[j] == key)
				return b.value[j];
		}

		return null;
	} // get

	/********************************************************************************
	 * Put the key-value pair in the hash table.
	 * 
	 * @author Guannan Wang
	 * @param key  the key to insert      
	 * @param value the value to insert
	 * @return null (not the previous value)
	 */
	public V put(K key, V value) {
		int i = h(key);

		// T O B E I M P L E M E N T E D
		
		out.print("Step " + count + ": ");
		count++;  // increase count
		this.print();
		
		if (hTable.get(i).status == 0) { // no split yet
			int slot = isBucketFull(hTable.get(i));
			if (slot == -1) { // full

				if (hTable.get(i).next == null) { // need to create an overflow page
					LinHashMap<K, V>.Bucket b = this.new Bucket(null);
					hTable.get(i).next = b;
					hTable.get(i).next.key[0] = key;
					hTable.get(i).next.value[0] = value;

				} else { // exists overflow page,
					// if overflow page is not full
					int flag = isBucketFull(hTable.get(i).next);
					if (flag == -1) {
					} else {
						hTable.get(i).next.key[flag] = key;
						hTable.get(i).next.value[flag] = value;
					}
				}

				// overflow trigger bucket split
				// find current split pointer and split it
				split(hTable.get(this.split));

			} else { // bucket has space, just insert and update nKeys
				hTable.get(i).key[slot] = key;
				hTable.get(i).value[slot] = value;
			}
		} else { // status = 1 which means the bucket has been split
			i = h2(key);
			int slot = isBucketFull(hTable.get(i));
			// printBucket(hTable.get(i));
			if (slot == -1) { // full
				Bucket overflow = hTable.get(i).next;

				// populate overflow bucket
				if (overflow == null) { // need to create an overflow page
					overflow = this.new Bucket(null);
					overflow.key[0] = key;
					overflow.value[0] = value;
					hTable.get(i).next = overflow;
				} else { // exists overflow page,
					// if overflow page is not full
					int flag = isBucketFull(overflow);
					if (flag == -1) {
					} else {
						overflow.key[flag] = key;
						overflow.value[flag] = value;
						hTable.get(i).next = overflow;
					}
				}

				// overflow trigger bucket split
				// find current split pointer and split it
				split(hTable.get(this.split));
			} else { // bucket has space, just insert and update nKeys
				hTable.get(i).key[slot] = key;
				hTable.get(i).value[slot] = value;
			}
			// printBucket(hTable.get(i));
		}

		return null;
	} // put

	/**
	 * Split a bucket
	 * @author Guannan Wang
	 * @param b Bucket to split
	 */
	private void split(Bucket b) {

		// add a new bucket to hTable list
		Bucket newBucket = this.new Bucket(null);
		newBucket.status = 1;
		hTable.add(newBucket);

		LinHashMap<K, V>.Bucket tempB = this.new Bucket(null);
		tempB.key = b.key;
		tempB.value = b.value;

		b.key = (K[]) Array.newInstance(classK, SLOTS);
		b.value = (V[]) Array.newInstance(classV, SLOTS);
		b.status = 1;

		for (int i = 0; i < tempB.key.length; i++) {

			if (tempB.key[i] != null) { // rehash
				this.put(tempB.key[i], tempB.value[i]);

			} else { // empty slot, do nothing
			}
		}

		if (b.next != null) { // exists overflow
			for (int j = 0; j < b.next.key.length; j++) {

				if (b.next.key[j] != null) { // rehash
					this.put(b.next.key[j], b.next.value[j]);

				}
			} // end for loop
		
			b.next = null; // reset overflow page
		}

		this.split++; // increase split pointer

		// adjust hashing function
		int sum = 0;
		for (int i = 0; i < hTable.size(); i++) {
			sum += hTable.get(i).status;
		}
		if (sum == hTable.size()) {
			for (int i = 0; i < hTable.size(); i++) {
				hTable.get(i).status = 0;
			}
			mod1 = mod2;
			mod2 = 2 * mod1;
			this.split = 0; // reset split pointer
		}

	}

	/********************************************************************************
	 * Return the size (SLOTS * number of home buckets) of the hash table.
	 * 
	 * @return the size of the hash table
	 */
	public int size() {
		return SLOTS * (mod1 + split);
	} // size

	/********************************************************************************
	 * Print the hash table.
	 * @author Guannan Wang
	 */
	private void print() {
		out.println("Hash Table (Linear Hashing)");
		out.println("-------------------------------------------");

		// T O B E I M P L E M E N T E D
		for (int i = 0; i < hTable.size(); i++) {
			Bucket b = hTable.get(i);
			out.print("Bucket " + i + ": ");
			printBucket(b);
		}

		out.println("-------------------------------------------");
	} // print

	/**
	 * print out each bucket content
	 * @author Guannan Wang
	 * @param b Bucket
	 * 
	 */
	private void printBucket(Bucket b) {

		for (int i = 0; i < SLOTS; i++) {
			out.print(b.key[i] + " (" + b.value[i] + ") | ");
		}

		if (b.next != null) {

			for (int j = 0; j < b.next.key.length; j++) {
				if (b.next.key[j] != null) {
					out.print(" -> " + b.next.key[j] + " (" + b.next.value[j] + ") | ");
				}
			}
		}
		out.println();

	}

	/********************************************************************************
	 * Hash the key using the low resolution hash function.
	 * 
	 * @param key
	 *            the key to hash
	 * @return the location of the bucket chain containing the key-value pair
	 */
	private int h(Object key) {
		return key.hashCode() % mod1;
	} // h

	/********************************************************************************
	 * Hash the key using the high resolution hash function.
	 * 
	 * @param key
	 *            the key to hash
	 * @return the location of the bucket chain containing the key-value pair
	 */
	private int h2(Object key) {
		return key.hashCode() % mod2;
	} // h2

	/**
	 * Check whether a bucket is full or not. Return -1 if it's full, otherwise
	 * return the index/position of available slot.
	 * 
	 * @param b
	 * @return -1 if full, otherwise index of available bucket slot
	 */
	private int isBucketFull(Bucket b) {
		for (int i = 0; i < SLOTS; ++i) {
			if (b.key[i] == null) {
				return i;
			}
		}
		return -1;
	}
	
    /********************************************************************************
     * The main method used for testing.
     * @param  the command-line arguments (args [0] gives number of keys to insert)
     */
    public static void main (String [] args)
    {
        LinHashMap <Integer, Integer> ht = new LinHashMap <> (Integer.class, Integer.class, 11);
        int nKeys = 30;
        if (args.length == 1) nKeys = Integer.valueOf (args [0]);
        for (int i = 1; i < nKeys; i += 2) ht.put (i, i * i);
        ht.print ();
        for (int i = 0; i < nKeys; i++) {
            out.println ("key = " + i + " value = " + ht.get (i));
        } // for
        out.println ("-------------------------------------------");
        out.println ("Average number of buckets accessed = " + ht.count / (double) nKeys);
    } // main

    

	/**
	 * Guannan Wang's test main function 
	 * 
	 * @author Guannan Wang 
	 * 
	 */
    /**
	public static void main(String[] args) {
		// LinHashMap <Integer, Integer> ht = new LinHashMap <> (Integer.class,
		// Integer.class, 11);
		LinHashMap<Integer, Integer> ht = new LinHashMap<>(Integer.class,
				Integer.class, 3);
		int nKeys = 30;
		if (args.length == 1)
			nKeys = Integer.valueOf(args[0]);

		int[] sK = new int[] { 1, 7, 3, 8, 12, 4, 11, 2, 10, 13 };

		for (int i = 0; i < sK.length; i += 1)
			ht.put(sK[i], sK[i] * sK[i]);
		ht.print();
		
		for (int i = 0; i < sK.length; i++) {
			 out.println ("key = " + sK[i] + " value = " + ht.get (sK[i]));
		} // for
		
		
		// test entrySet()
		Iterator it = ht.entrySet().iterator();
		out.println("-------------------------------------------");
		out.println("All entrySet - test entrySet function");
		while(it.hasNext()) {
			out.println(it.next());
		}
		
		out.println("-------------------------------------------");
		out.println("Average number of buckets accessed = " + ht.count
				/ (double) nKeys);
	} // main
	*/

} // LinHashMap class

