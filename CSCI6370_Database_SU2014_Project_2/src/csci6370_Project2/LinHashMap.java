package csci6370_Project2;



/************************************************************************************
 * @file LinHashMap.java
 *
 * @author  John Miller
 */

import java.io.*;
import java.lang.reflect.Array;

import static java.lang.System.out;

import java.util.*;

/************************************************************************************
 * This class provides hash maps that use the Linear Hashing algorithm.
 * A hash table is created that is an array of buckets.
 */
public class LinHashMap <K, V>
       extends AbstractMap <K, V>
       implements Serializable, Cloneable, Map <K, V>
{
    /** The number of slots (for key-value pairs) per bucket.
     */
    private static final int SLOTS = 4;

    /** The class for type K.
     */
    private final Class <K> classK;

    /** The class for type V.
     */
    private final Class <V> classV;

    /********************************************************************************
     * This inner class defines buckets that are stored in the hash table.
     */
    private class Bucket
    {
        int    nKeys;
        K []   key;
        V []   value;
        Bucket next;
        //initial attempt by Arya
        int Depth; 
        @SuppressWarnings("unchecked")
        Bucket (Bucket n)
        //experimental
        //Bucket()
        {
            nKeys = 0;
            key   = (K []) Array.newInstance (classK, SLOTS);
            value = (V []) Array.newInstance (classV, SLOTS);
            next  = n;
            Depth = 2; // say for now!
        } // constructor
        
       // remove all elements
        void removeAllElements() {
        	nKeys = 0;
        }
    } // Bucket inner class

    /** The list of buckets making up the hash table.
     */
    private final List <Bucket> hTable;

    /** The modulus for low resolution hashing
     */
    private int mod1;

    /** The modulus for high resolution hashing
     */
    private int mod2;

    /** Counter for the number buckets accessed (for performance testing).
     */
    private int count = 0;

    /** The index of the next bucket to split.
     */
    private int split = 0;
    
    // added by Arya.
    /** The number of buckets
	 */
	private int nBuckets;
	
	/** The global depth of the hash tree
	 * 
	 */
	public int gDepth=2;
	
	
    /********************************************************************************
     * Construct a hash table that uses Linear Hashing.
     * @param classK    the class for keys (K)
     * @param classV    the class for keys (V)
     * @param initSize  the initial number of home buckets (a power of 2, e.g., 4)
     * @author Aryabrata Basu
     */
    public LinHashMap (Class <K> _classK, Class <V> _classV, int initSize)
    {
        classK = _classK;
        classV = _classV;
        hTable = new ArrayList <> ();
        // modifed with nBuckets
        mod1  = nBuckets = initSize;
        mod2   = 2 * mod1;
        
       //initial attempt by Arya
       for(int i=0; i<initSize; i++) {
    	   Bucket b = new Bucket(null);
    	   hTable.add(b);
       }
    } // constructor

    /********************************************************************************
     * Return a set containing all the entries as pairs of keys and values.
     * @return  the set view of the map
     * @author Aryabrata Basu
     */
    public Set <Map.Entry <K, V>> entrySet ()
    {
        Set <Map.Entry <K, V>> enSet = new HashSet <> ();

        //  T O   B E   I M P L E M E N T E D
        // initial attempt by Arya
        for(int i=0;i<nBuckets;i++) {
        		for(int j=0;j<hTable.get(i).nKeys;j++) {
        			enSet.add(new AbstractMap.SimpleEntry(hTable.get(i).key[j],hTable.get(i).value[j]));
        		}
        }
            
        return enSet;
    } // entrySet

    /********************************************************************************
     * Given the key, look up the value in the hash table.
     * @param key  the key used for look up
     * @return  the value associated with the key
     * @author Aryabrata Basu
     */
    public V get (Object key)
    {
        int i = h (key);

        //  T O   B E   I M P L E M E N T E D
        // initial attempt by Arya
        Bucket b = hTable.get(i);
        for(int j=0;j<b.nKeys;j++) {
        	if(b.key[j].toString().equals(key.toString())) {
        		return b.value[j];
        	}
        }
        

        return null;
    } // get

    /********************************************************************************
     * Put the key-value pair in the hash table.
     * @param key    the key to insert
     * @param value  the value to insert
     * @return  null (not the previous value)
     * @author Aryabrata Basu
     */
    public V put (K key, V value)
    {
        // added by Arya - debug
    	System.out.println("Inserting Value (" + value + ") and key (" + key + ")");
    	
    	int i = h (key);

        //  T O   B E   I M P L E M E N T E D
        // initial attempt by Arya
        int loopy = 0;
        Bucket b = hTable.get(i);
        
        
        // partial split logic embedded below
        while(b.nKeys == SLOTS) {
        	Bucket newlyCreatedBucket = new Bucket(null); 
    		hTable.add(newlyCreatedBucket); 
    		
    		if(b.Depth == gDepth) {
    			for(loopy = 0;loopy<mod1;loopy++) {
    				hTable.add(hTable.get(loopy));
    			}
    			
    			hTable.set(i+mod1,newlyCreatedBucket);
    			//mod1 = mod1 *2;
    			mod1 = mod2;
    			nBuckets = nBuckets *2;
    			gDepth++;
    			
    		}
    		else {
    			hTable.set(i,newlyCreatedBucket);
    		}
    		
    		b.Depth++;
    		newlyCreatedBucket.Depth = b.Depth;
    		
    		K[] tempKey   = (K []) Array.newInstance (classK, SLOTS);
			V[] tempValue = (V []) Array.newInstance (classV, SLOTS);
			
			for(loopy=0;loopy<SLOTS;loopy++) {
				tempKey[loopy] = b.key[loopy];
				tempValue[loopy]  = b.value[loopy]; 
			}
			
			b.removeAllElements();
			
			for(loopy=0;loopy<SLOTS;loopy++) {
				i = h(tempKey[loopy]);
				b = hTable.get(i);
				
				try {
					b.key[b.nKeys] = tempKey[loopy];
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				b.value[b.nKeys] = tempValue[loopy];
				
				b.nKeys++;
			}
			
			i = h(key);
			b= hTable.get(i);
		}
        
        b.key[b.nKeys] = key;
        b.value[b.nKeys] = value;
        b.nKeys++;
        
        return value;
        //return null;
    } // put

    /********************************************************************************
     * Return the size (SLOTS * number of home buckets) of the hash table. 
     * @return  the size of the hash table
     */
    public int size ()
    {
        return SLOTS * (mod1 + split);
    } // size

    /********************************************************************************
     * Print the hash table.
     */
    private void print ()
    {
        out.println ("Hash Table (Linear Hashing)");
        out.println ("-------------------------------------------");

        //  T O   B E   I M P L E M E N T E D
        // initial attempt by Arya
        
        for(int i = 0; i<nBuckets; i++) { 
			System.out.print(i + " [ "); 
			for(int j = 0; j<hTable.get(i).nKeys; j++) { 
				System.out.print("(" + hTable.get(i).key[j] + " " + hTable.get(i).value[j] + ") "); 
			}
			System.out.println("]"); 
			
		}

        out.println ("-------------------------------------------");
    } // print

    /********************************************************************************
     * Hash the key using the low resolution hash function.
     * @param key  the key to hash
     * @return  the location of the bucket chain containing the key-value pair
     */
    private int h (Object key)
    {
        return key.hashCode () % mod1;
    } // h

    /********************************************************************************
     * Hash the key using the high resolution hash function.
     * @param key  the key to hash
     * @return  the location of the bucket chain containing the key-value pair
     */
    private int h2 (Object key)
    {
        return key.hashCode () % mod2;
    } // h2

    /********************************************************************************
     * The main method used for testing.
     * @param  the command-line arguments (args [0] gives number of keys to insert)
     */
//    public static void main (String [] args)
//    {
//        LinHashMap <Integer, Integer> ht = new LinHashMap <> (Integer.class, Integer.class, 11);
//        int nKeys = 30;
//        if (args.length == 1) nKeys = Integer.valueOf (args [0]);
//        for (int i = 1; i < nKeys; i += 2) ht.put (i, i * i);
//        ht.print ();
//        for (int i = 0; i < nKeys; i++) {
//            out.println ("key = " + i + " value = " + ht.get (i));
//        } // for
//        out.println ("-------------------------------------------");
//        out.println ("Average number of buckets accessed = " + ht.count / (double) nKeys);
//    } // main
    
    
    // this is for test cases - Arya
    /**
     * This is an alternative main method 
     * @param args
     * @author Aryabrata Basu
     */
    public static void main (String [] args) {
    	LinHashMap <Integer, Integer> aLinHash = new LinHashMap <> (Integer.class, Integer.class,SLOTS);
    	
    	int nKeys = 30;
    	if(args.length == 1) {
    		nKeys = Integer.valueOf(args[0]);
    	}
    		
//		may be later - introduce randomness
//    	int randomI = new Random().nextInt(10);
//		int randomVal = new Random().nextInt(10);
	
		int[] array = new int[] { 1,1,2,3,5,8,13,21,34,55,89,144 }; // fibonacci sequence

		
		for (int i=0; i<array.length;i++) {
			aLinHash.put(array[i], array[i] * array[i]);
			
		}
		
		aLinHash.print();
		//System.out.println(aLinHash.size());
		
		out.println ("-------------------------------------------");
		out.println ("Average number of buckets accessed = " + aLinHash.size() / (double) nKeys);
    
    } // alternate main()
    
   
} // LinHashMap class

