<jsp:include page="header.jsp" />
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 <%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body background="http://geolog.engr.uga.edu/crimeDBFiles/backGround-1.jpg">



<s:if test="policeList.size() > 0">

All police members:<br/>


<table border="1">
<tr>
    <th>Police Id</th>
    <th>First Name</th>
    <th>Last Name</th>
    <th>Department</th>
    <th>Contact</th>
    <th>Gender</th>
     <th>Specialties</th>
    <th>&nbsp;</th>
 </tr>
<s:iterator value="policeList">

   <tr>

<td><s:property value="policeId"/></td> 
<td><s:property value="firstName"/></td> 
<td><s:property value="lastName"/></td>  
<td><s:property value="department"/></td>  
<td><s:property value="contactNumber"/></td>  
<td><s:property value="gender"/></td>
<td><s:property value="specialtyNames"/></td>
 <td class="nowrap">
 
              
                <s:url action="editPolice" id="editUrl">
                    <s:param name="policeId" value="%{policeId}"/>
                </s:url>
                <s:a href="%{editUrl}">Edit</s:a>
             
                <s:url action="deletePolice" id="deleteUrl">
                    <s:param name="policeId" value="policeId"/>
                </s:url>
                <s:a href="%{deleteUrl}">Delete</s:a>
 </td>

       </tr> 
 
</s:iterator>
</table><br/>



</s:if>





</body>
</html>
