<jsp:include page="header.jsp" />
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Case Details</title>
</head>

<!-- <script language="javascript" type="text/javascript">  
  
function doSubmit()  
{  
window.open("getCriminals");  
}  
</script> -->
<body background="http://geolog.engr.uga.edu/crimeDBFiles/backGround-1.jpg">

<h1>Is criminal <s:property value="crimIdSelected"/> involved in other cases? </h1>
<s:form action="addOtherCases">
   <s:checkboxlist label="case Ids" name="otherCaseIds" list="otherCases" />
   <s:hidden name="crimIdSelected" />
   
   <s:submit value="File a case" align="right"/>	
 </s:form>
 </body>
 </html>