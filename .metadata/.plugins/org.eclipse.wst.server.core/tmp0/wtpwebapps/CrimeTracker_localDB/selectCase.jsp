<jsp:include page="header.jsp" />
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Case Details</title>
</head>
<body>
<h1>Case Details</h1>
  
<table border="1">
<tr>
    <th>CaseID</th>
    <th>Description</th>
    <th>Photos</th>
 </tr>
<s:iterator value="caseList">

    <tr>

<td><s:property value="caseId"/></td> 
<td><s:property value="description"/></td>  
<td><a href="findPhotoByCase?caseSelected=<s:property value="caseId"/>">Photos</a></td> 
       </tr> 
 
</s:iterator>

</table><br/>




</body>
</html>