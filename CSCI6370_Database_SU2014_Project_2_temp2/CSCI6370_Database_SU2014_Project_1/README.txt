################################################################################################
PROJECT: DATABASE PROJECT 1
################################################################################################
TEAM MEMBERS
################################################################################################

ARYABRATA BASU (MANAGER)
ALEKHYA CHENNUPATI
ALEXIS LEIMA
GUANNAN WANG
BHARGABI CHAKRABARTI

################################################################################################

STEPS to execution:
____________________

1) Unzip CSCI6370_Database_SU2014_Project_1.zip.

2) Open a prompt/terminal window.

3) Change your current directory to CSCI6370_Database_SU2014_Project_1 (the build.xml, the
   src, bin, javadoc etc. folders should be present in this directory).

4) Class diagram is included and resides inside CSCI6370_Database_SU2014_Project_1 directory.

5) To compile run the command:
   ant build

6) To run the project, use command:
   ant MovieDB

7) To generate javadocs, run command:
   ant javadoc

8) To run our test cases, run command:
   ant TestCases

NOTE: MAKE SURE YOU USE ANT 1.9 OR HIGHER, LOWER VERSIONS DO NOT SUPPORT JAVA 8.

ANOTHER NOTE: To execute your own test cases, one should handle null values. for example:
		
	A typical Join test case might look like this:
		
	///////////////////////////////////////////////////////////
	Table t_join3 = movie.join ("title", "title year", cinema);
	if(t_join3 != null)
        t_join3.print ();
	///////////////////////////////////////////////////////////

	This is because returning non-null or table without tuples DOES NOT make sense, if JOIN does not happen.

