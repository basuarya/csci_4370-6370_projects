package csci6370_Project2;




/*****************************************************************************************
 * @file  MovieDB.java
 *
 * @author   John Miller
 

*/
import static java.lang.System.out;

/*****************************************************************************************
 * The MovieDB class makes a Movie Database.  It serves as a template for making other
 * databases.  See "Database Systems: The Complete Book", second edition, page 26 for more
 * information on the Movie Database schema.
 */
public class TestCases_Project1
{
    /*************************************************************************************
     * Main method for creating, populating and querying a Movie Database.
     * @param args  the command-line arguments
     */
    public static void main (String [] args)
    {
        out.println ();

        Table movie = new Table ("movie", "title year length genre studioName producerNo",
                                          "String Integer Integer String String Integer", "title year");

        Table cinema = new Table ("cinema", "title year length genre studioName producerNo",
                                            "String Integer Integer String String Integer", "title year");

        Table movieStar = new Table ("movieStar", "name address gender birthdate",
                                                  "String String Character String", "name");

        Table starsIn = new Table ("starsIn", "movieTitle movieYear starName",
                                              "String Integer String", "movieTitle movieYear starName");

        Table movieExec = new Table ("movieExec", "certNo name address fee",
                                                  "Integer String String Float", "certNo");

        Table studio = new Table ("studio", "name address presNo",
                                            "String String Integer", "name");

        Comparable [] film0 = { "Star_Wars", 1977, 124, "sciFi", "Fox", 12345 };
        Comparable [] film1 = { "Star_Wars_2", 1980, 124, "sciFi", "Fox", 12345 };
        Comparable [] film2 = { "Rocky", 1985, 200, "action", "Universal", 12125 };
        Comparable [] film3 = { "Rambo", 1978, 100, "action", "Universal", 32355 };
        
        // team generated test case
        Comparable [] film_dummy = { "Rambo", 1978, 100, "action", "Universal", 32355, "dummy" };
        
        out.println ();
        movie.insert (film0);
        movie.insert (film1);
        movie.insert (film2);
        movie.insert (film3);
        
        // team generated test case
        movie.insert(film_dummy);
        
        movie.print ();

        Comparable [] film4 = { "Galaxy_Quest", 1999, 104, "comedy", "DreamWorks", 67890 };
        out.println ();
        cinema.insert (film2);
        cinema.insert (film3);
        cinema.insert (film4);
        cinema.print ();

        Comparable [] star0 = { "Carrie_Fisher", "Hollywood", 'F', "9/9/99" };
        Comparable [] star1 = { "Mark_Hamill", "Brentwood", 'M', "8/8/88" };
        Comparable [] star2 = { "Harrison_Ford", "Beverly_Hills", 'M', "7/7/77" };
        out.println ();
        movieStar.insert (star0);
        movieStar.insert (star1);
        movieStar.insert (star2);
        movieStar.print ();

        Comparable [] cast0 = { "Star_Wars", 1977, "Carrie_Fisher" };
        out.println ();
        starsIn.insert (cast0);
        starsIn.print ();

        Comparable [] exec0 = { 9999, "S_Spielberg", "Hollywood", 10000.00 };
        out.println ();
        movieExec.insert (exec0);
        movieExec.print ();

        Comparable [] studio0 = { "Fox", "Los_Angeles", 7777 };
        Comparable [] studio1 = { "Universal", "Universal_City", 8888 };
        Comparable [] studio2 = { "DreamWorks", "Universal_City", 9999 };
        out.println ();
        studio.insert (studio0);
        studio.insert (studio1);
        studio.insert (studio2);
        studio.print ();

        movie.save ();
        cinema.save ();
        movieStar.save ();
        starsIn.save ();
        movieExec.save ();
        studio.save ();

        movieStar.printIndex ();

        //--------------------- project

        out.println ();
        Table t_project1 = movie.project ("title year");
        if(t_project1 != null)
        t_project1.print ();
        
        
        // team generated test case
        Table t_project2 = movie.project ("name year");
        if(t_project2 != null)
        t_project2.print ();

        //--------------------- select

         out.println ();
        Table t_select1 = movie.select (t -> t[movie.col("title")].equals ("Star_Wars") &&
                                            t[movie.col("year")].equals (1977));
        if(t_select1 != null)
        t_select1.print (); 
        
        // team generated test case
        Table t_select2 = movie.select (t -> t[movie.col("title")].equals ("Star") &&
                t[movie.col("year")].equals (1977));
        if(t_select2 != null)
        t_select2.print (); 

        //--------------------- indexed select

        out.println ();
        Table t_iselect1 = movieStar.select (new KeyType ("Harrison_Ford"));
        if(t_iselect1 != null)
        t_iselect1.print ();
        
        
        // team generated test case
        Table t_iselect2 = movieStar.select (new KeyType ("TeamDB"));
        if(t_iselect2 != null)
        t_iselect2.print ();

        //--------------------- union

        out.println ();
        Table t_union1 = movie.union (cinema);
        if(t_union1 != null)
        t_union1.print ();
        
        // team generated test case
        Table t_union2 = movie.union (movieStar);
        if(t_union2 != null)
        t_union2.print ();
        
        //--------------------- minus

        out.println ();
        Table t_minus1 = movie.minus (cinema);
        if(t_minus1 != null)
        t_minus1.print ();
        
        // team generated test case
        Table t_minus2 = movie.minus (movieStar);
        if(t_minus2 != null)
        t_minus2.print ();
        

        //--------------------- join
       
        out.println ();
        Table t_join1 = movie.join ("studioName", "name", studio);
        if(t_join1 != null)
        t_join1.print ();

        out.println ();
        
        Table t_join2 = movie.join ("title year", "title year", cinema);
        if(t_join2 != null)
        t_join2.print ();
        
        out.println ();
        // team generated test case
        Table t_join3 = movie.join ("title", "title year", cinema);
        if(t_join3 != null)
            t_join3.print ();
        

    } // main



}